package jp.gr.java_conf.knystd.vermont.fukujin;

import jp.gr.java_conf.knystd.vermont.VermontExecutor;

public class Form extends Widget {

    public Form(int x, int y, int width, int height) {
        super(null, x, y, width, height);
        this.m_modal = null;
    }

    public Form getModal() {
        return this.m_modal;
    }

    public void onInit() {
    }

    public void onClose() {
    }

    public void showModal(Form form) {
        form.onInit();
        this.m_modal = form;
    }

    public void closeModal() {
        this.m_modal.onClose();
        this.m_modal = null;
    }

    @Override
    public void dispatchUpdate() {
        super.dispatchUpdate();

        if (this.m_modal != null) {
            this.m_modal.dispatchUpdate();
        }
    }

    @Override
    public void dispatchDraw(VermontExecutor ex) {
        super.dispatchDraw(ex);

        if (this.m_modal != null && this.m_modal.isVisible()) {
            this.m_modal.dispatchDraw(ex);
        }
    }

    @Override
    public void dispatchTouchEvent(int action, int count, int[] coord, int[] id) {
        super.dispatchTouchEvent(action, count, coord, id);

        if (this.m_modal != null) {
            this.m_modal.dispatchTouchEvent(action, count, coord, id);
        }
    }

    public boolean onBackPressed() {
        return true;
    }

    protected Form m_modal;

}
