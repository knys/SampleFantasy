package jp.gr.java_conf.knystd.samplefantasy;
import android.app.Activity;

import java.util.ArrayList;
import java.util.Random;

import jp.gr.java_conf.knystd.vermont.*;
import jp.gr.java_conf.knystd.vermont.fukujin.*;
import jp.gr.java_conf.knystd.vermont.util.*;


/*------------------------------------------------------------------------------ */
/* TitleForm */
/* タイトル画面 */
/*------------------------------------------------------------------------------ */
public class TitleForm extends Form {

    public TitleForm(int x, int y, int width, int height, SampleFantasyApp app) {
        super(x, y, width, height);

        this.m_app = app;

        /* 画面にボタンを追加する */
        this.addChild(new TitleButton(this, 350, 410, 100, 60, 1, app));
        this.addChild(new TitleButton(this, 350, 510, 100, 60, 2, app));
        this.addChild(new TitleButton(this, 350, 610, 100, 60, 3, app));
    }

    @Override
    public void onTouchEvent(int action, int count, int[] coord, int[] id) {
    }

    @Override
    public void onDraw(VermontExecutor ex) {
        /* タイトル描画 */
        ex.cls(ex.rgb(24, 48, 96));
        int x = this.getDispX();
        int y = this.getDispY();
        ex.blt(x, y + 120, SampleFantasyApp.IMG_MAIN, 543, 883, 480, 140);

        ex.blt(x + 20, y + 410, SampleFantasyApp.IMG_MAIN, 365, 495, 120, 60);
        ex.blt(x, y + 390, SampleFantasyApp.IMG_MAIN, 543, 522, 480, 100);
        DrawUtil.drawTime(ex, x + 180, y + 440, this.m_app.getBestTime(1));
        ex.blt(x + 20, y + 510, SampleFantasyApp.IMG_MAIN, 365, 557, 120, 60);
        ex.blt(x, y + 490, SampleFantasyApp.IMG_MAIN, 543, 522, 480, 100);
        DrawUtil.drawTime(ex, x + 180, y + 540, this.m_app.getBestTime(2));
        ex.blt(x + 20, y + 610, SampleFantasyApp.IMG_MAIN, 365, 619, 120, 60);
        ex.blt(x, y + 590, SampleFantasyApp.IMG_MAIN, 543, 522, 480, 100);
        DrawUtil.drawTime(ex, x + 180, y + 640, this.m_app.getBestTime(3));
    }

    @Override
    public boolean onBackPressed() {
        /* タイトル画面でバックボタンを押したら終了する */
        return true;
    }

    protected SampleFantasyApp m_app;
}
