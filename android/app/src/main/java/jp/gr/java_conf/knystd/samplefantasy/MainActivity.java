package jp.gr.java_conf.knystd.samplefantasy;

import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

import jp.gr.java_conf.knystd.vermont.VermontActivity;

public class MainActivity extends VermontActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        init(new SampleFantasyExecutor(), true, false);
    }
}
