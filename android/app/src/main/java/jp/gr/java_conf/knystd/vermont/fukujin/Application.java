package jp.gr.java_conf.knystd.vermont.fukujin;

import jp.gr.java_conf.knystd.vermont.VermontExecutor;

public class Application {

    public Application(VermontExecutor executor) {
        this.m_executor = executor;
        this.m_form = null;
        this.m_capture = null;
    }

    public VermontExecutor getExecutor() {
        return this.m_executor;
    }

    public Form getForm() {
        return this.m_form;
    }

    public void setForm(Form form) {
        if (this.m_form != null) {
            this.m_form.onClose();
        }

        this.m_form = form;
        if (form != null) {
            form.onInit();
        }
    }

    public void onTouchEvent(int action, int count, int[] coord, int[] id) {
        Form form = this.m_form;

        if (this.m_capture != null) {
            /* キャプチャ中のウィジェットがあれば優先 */
            /* LongEarなWidgetのonTouchEventは後で実行する */
            if (!this.m_capture.isLongEar()) {
                this.m_capture.onTouchEvent(action, count, coord, id);
            }
        } else if (action == VermontExecutor.ACTION_DOWN) {
            if (form != null) {
                /* Modalな子Formがある場合は、そちらを優先する */
                while (form.getModal() != null) {
                    form = form.getModal();
                }

                this.m_capture = form.getHitWidget(action, count, coord, id);
                /* LongEarなWidgetのonTouchEventは後で実行する */
                if (this.m_capture != null && !this.m_capture.isLongEar()) {
                    this.m_capture.onTouchEvent(action, count, coord, id);
                }
            }
        }

        /* すべてのLongEarなWidgetのonTouchEventを実行する */
        if (form != null) {
            form.dispatchTouchEvent(action, count, coord, id);
        }

        /* 指が離れたらキャプチャ終了 */
        if (action == VermontExecutor.ACTION_UP) {
            this.m_capture = null;
        }
    }

    public void onUpdate() {
        if (this.m_form != null) {
            this.m_form.dispatchUpdate();
        }
    }

    public void onDraw() {
        if (this.m_form != null && this.m_form.isVisible()) {
            this.m_form.dispatchDraw(this.m_executor);
        }
    }

    public boolean onBackPressed() {
        if (this.m_form == null) {
            return true;
        }

        Form f1 = this.m_form;
        Form f2 = f1.getModal();
        if (f2 == null) {
            /* Modalな子Formがない場合は、このFormのonBackPressedの結果を返す */
            return f1.onBackPressed();
        } else {
            /* Modalな子Formがある場合は、子を辿ってアクティブなFormを探す */
            while (f2.getModal() != null) {
                f1 = f2;
                f2 = f2.getModal();
            }

            /* ModalのonBackPressedの結果でModalを閉じる */
            if (f2.onBackPressed()) {
                f1.closeModal();
            }

            return false;
        }
    }

    protected VermontExecutor m_executor;
    protected Form m_form;
    protected Widget m_capture;

}
