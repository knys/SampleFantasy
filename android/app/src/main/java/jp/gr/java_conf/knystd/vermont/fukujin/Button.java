package jp.gr.java_conf.knystd.vermont.fukujin;

import jp.gr.java_conf.knystd.vermont.VermontExecutor;

public class Button extends Widget {

    public Button(Widget parent, int x, int y, int width, int height) {
        super(parent, x, y, width, height);
        this.m_focused = false;
    }

    @Override
    public void onTouchEvent(int action, int count, int[] coord, int[] id) {
        if (action == VermontExecutor.ACTION_DOWN) {
            this.m_focused = true;
        } else if (action == VermontExecutor.ACTION_MOVE) {
            if (!this.isHit(action, count, coord, id)) {
                this.m_focused = false;
            }
        } else if (action == VermontExecutor.ACTION_UP) {
            if (this.m_focused && this.isHit(action, count, coord, id)) {
                this.onClick();
            }
            this.m_focused = false;
        }
    }

    @Override
    public void onDraw(VermontExecutor ex) {
        int x = this.getDispX();
        int y = this.getDispY();
        int w = this.getWidth();
        int h = this.getHeight();

        int c = ex.setColor(ex.rgb(255, 255, 255));
        ex.drawRect(x, y, w, h);
        ex.setColor(c);
    }

    public boolean isFocused() {
        return this.m_focused;
    }

    public void onClick() {
    }

    protected boolean m_focused;

}
