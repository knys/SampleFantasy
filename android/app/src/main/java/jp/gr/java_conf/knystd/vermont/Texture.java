package jp.gr.java_conf.knystd.vermont;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.opengl.GLES20;
import android.opengl.GLUtils;
import android.os.Build;

class Texture {
    private enum TextureState {UNUSED, READY}

    private TextureState m_textureState;
    private int m_id;
    private Bitmap m_bitmap;
    private int m_width;
    private int m_height;
    private int m_textureId;
    private Resources m_res;

    public Texture(Resources res) {
        m_textureState = TextureState.UNUSED;
        m_id = -1;
        m_bitmap = null;
        m_width = 0;
        m_height = 0;
        m_textureId = 0;
        m_res = res;
    }

    void init() {
        if (m_textureId == 0) {
            int[] textures = new int[1];
            GLES20.glGenTextures(1, textures, 0);
            m_textureId = textures[0];
        }
    }

    boolean isUsable() {
        return m_textureState == TextureState.UNUSED;
    }

    int getWidth() {
        return m_width;
    }

    int getHeight() {
        return m_height;
    }

    int getTextureId() {
        return m_textureId;
    }

    boolean load(int id) {
        if (m_bitmap != null)
            delete();
        if (id == -1)
            return false;

        m_id = id;
        if (Build.VERSION.SDK_INT < 19) {
            m_bitmap = BitmapFactory.decodeResource(m_res, id);
        } else {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPremultiplied = false;
            m_bitmap = BitmapFactory.decodeResource(m_res, id, options);
        }
        if (m_bitmap == null)
            return false;
        m_width = m_bitmap.getWidth();
        m_height = m_bitmap.getHeight();

        createTexture();

        return true;
    }

    void delete() {
        if (m_bitmap != null) {
            m_bitmap.recycle();
            m_bitmap = null;
        }

        m_id = -1;
        m_width = 0;
        m_height = 0;

        deleteTexture();
    }

    void reload() {
        if (m_textureState == TextureState.READY) {
            load(m_id);
        }
    }

    private void createTexture() {
        GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, m_textureId);
        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_NEAREST);
        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR);
//		GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_NEAREST);
        GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_S, GLES20.GL_CLAMP_TO_EDGE);
        GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_T, GLES20.GL_CLAMP_TO_EDGE);

        GLUtils.texImage2D(GLES20.GL_TEXTURE_2D, 0, m_bitmap, 0);
        m_bitmap.recycle();
        m_bitmap = null;
        m_textureState = TextureState.READY;
    }

    private void deleteTexture() {
        int[] textures = new int[1];
        textures[0] = m_textureId;
        GLES20.glDeleteTextures(1, textures, 0);
        m_textureState = TextureState.UNUSED;
    }
}
