package jp.gr.java_conf.knystd.samplefantasy;
import android.app.Activity;

import java.util.ArrayList;
import java.util.Random;

import jp.gr.java_conf.knystd.vermont.*;
import jp.gr.java_conf.knystd.vermont.fukujin.*;
import jp.gr.java_conf.knystd.vermont.util.*;


/*------------------------------------------------------------------------------ */
/* TitleButton */
/* タイトル画面用ボタン */
/*------------------------------------------------------------------------------ */
public class TitleButton extends Button {

    public TitleButton(Widget parent, int x, int y, int width, int height, int level, SampleFantasyApp app) {
        super(parent, x, y, width, height);
        this.m_level = level;
        this.m_app = app;
    }

    @Override
    public void onDraw(VermontExecutor ex) {
        /* ボタンの描画 */
        int x = this.getDispX();
        int y = this.getDispY();
        int w = this.getWidth();
        int h = this.getHeight();

        if (this.isFocused()) {
            x = x - 5;
            y = y - 5;
            w = w + 10;
            h = h + 10;
        }

        ex.stretchBlt(x, y, w, h, SampleFantasyApp.IMG_MAIN, 365, 433, 100, 60);
    }

    @Override
    public void onClick() {
        /* クリックされたらゲーム画面に切り替える */
        this.m_app.setLevel(this.m_level);
        this.m_app.setScene(SampleFantasyApp.SCENE_GAME);
    }

    protected int m_level;
    protected SampleFantasyApp m_app;
}
