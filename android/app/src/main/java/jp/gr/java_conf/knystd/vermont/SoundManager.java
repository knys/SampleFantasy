package jp.gr.java_conf.knystd.vermont;

import android.content.res.AssetFileDescriptor;
import android.content.res.Resources;
import android.content.res.Resources.NotFoundException;
import android.media.AudioManager;
import android.media.SoundPool;

import java.io.IOException;

class SoundManager {
    private class Sound {
        private int m_id;
        private int m_priority;

        int getId() {
            return m_id;
        }

        void setId(int id) {
            m_id = id;
        }

        int getPriority() {
            return m_priority;
        }

        void setPriority(int priority) {
            m_priority = priority;
        }

    }

    private Resources m_res;
    private SoundPool m_soundPool = null;
    private Sound[] m_sound = null;
    private int[] m_loopStreamId = null;
    private int m_numLoopStream = 0;
    private int m_soundMax;
    private int m_playMax;
    private boolean m_enabled;

    SoundManager(Resources res, int soundMax, int playMax) {
        m_res = res;
        m_soundMax = soundMax;
        m_playMax = playMax;
    }

    void init() {
        if (m_soundPool != null)
            release();

        m_soundPool = new SoundPool(m_playMax, AudioManager.STREAM_MUSIC, 0);

        m_sound = new Sound[m_soundMax];
        for (int i = 0; i < m_sound.length; i++) {
            m_sound[i] = null;
        }

        m_loopStreamId = new int[m_playMax];
        m_numLoopStream = 0;
    }

    void release() {
        if (m_soundPool != null) {
            m_soundPool.release();
            m_soundPool = null;
        }
        m_sound = null;
        m_loopStreamId = null;
        m_numLoopStream = 0;
    }

    void enable(boolean enabled) {
        m_enabled = enabled;
        if (!enabled) {
            stopAll();
        }
    }

    int load(int id, int priority, int index) {
        if (index < -1 || index >= m_sound.length)
            return -1;

        // 空きを探す
        if (index < 0) {
            for (int i = 0; i < m_sound.length; i++) {
                if (m_sound[i] == null) {
                    index = i;
                    break;
                }
            }
        }
        // 空きがない場合
        if (index < 0)
            return -1;

        if (m_sound[index] == null) {
            m_sound[index] = new Sound();
        }

        AssetFileDescriptor afd = null;
        try {
            afd = m_res.openRawResourceFd(id);
            m_sound[index].setId(m_soundPool.load(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength(), 1));
            m_sound[index].setPriority(priority);
        } catch (NotFoundException e) {
            return -1;
        } finally {
            try {
                afd.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return index;
    }

    void free(int index) {
        if (index < 0 || index >= m_sound.length || m_sound[index] == null)
            return;

        m_soundPool.unload(m_sound[index].getId());
    }

    int play(int index, boolean looping, float leftVolume, float rightVolume, float rate) {
        if (!m_enabled || index < 0 || index >= m_sound.length || m_sound[index] == null)
            return 0;

        int priority;
        if (looping) {
            priority = 100;
        } else {
            priority = m_sound[index].getPriority();
        }

        int id = m_soundPool.play(m_sound[index].getId(), leftVolume, rightVolume, priority, looping ? -1 : 0, rate);
        if (looping) {
            addLoopStreamId(id);
        }
        return id;
    }

    void pause(int id) {
        if (!m_enabled)
            return;

        m_soundPool.pause(id);
        removeLoopStreamId(id);
    }

    void resume(int id) {
        if (!m_enabled)
            return;

        m_soundPool.resume(id);
        addLoopStreamId(id);
    }

    void stop(int id) {
        if (!m_enabled)
            return;

        m_soundPool.stop(id);
        removeLoopStreamId(id);
    }

    void setRate(int id, float rate) {
        if (!m_enabled)
            return;

        m_soundPool.setRate(id, rate);
    }

    void setVolume(int id, float lvol, float rvol) {
        if (!m_enabled)
            return;

        m_soundPool.setVolume(id, lvol, rvol);
    }

    void stopAll() {
        for (int i = 0; i < m_numLoopStream; i++) {
            m_soundPool.stop(m_loopStreamId[i]);
        }
        m_numLoopStream = 0;
    }

    void onPause() {
        for (int i = 0; i < m_numLoopStream; i++) {
            m_soundPool.pause(m_loopStreamId[i]);
        }
    }

    void onResume() {
        if (m_enabled) {
            for (int i = 0; i < m_numLoopStream; i++) {
                m_soundPool.resume(m_loopStreamId[i]);
            }
        } else {
            stopAll();
        }
    }

    private void addLoopStreamId(int id) {
        if (m_numLoopStream >= m_loopStreamId.length) {
            // 古いStreamIDを削除
            for (int i = 0; i < m_loopStreamId.length - 2; i++) {
                m_loopStreamId[i] = m_loopStreamId[i + 1];
            }
            m_numLoopStream = m_loopStreamId.length - 1;
        }
        m_loopStreamId[m_numLoopStream] = id;
        m_numLoopStream++;
    }

    private void removeLoopStreamId(int id) {
        boolean isExist = false;
        for (int i = 0; i < m_numLoopStream; i++) {
            if (m_loopStreamId[i] == id) {
                isExist = true;
                m_numLoopStream--;
            }

            if (isExist && i < m_numLoopStream)
                m_loopStreamId[i] = m_loopStreamId[i + 1];
        }
    }
}
