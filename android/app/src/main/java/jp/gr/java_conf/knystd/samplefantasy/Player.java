package jp.gr.java_conf.knystd.samplefantasy;
import android.app.Activity;

import java.util.ArrayList;
import java.util.Random;

import jp.gr.java_conf.knystd.vermont.*;
import jp.gr.java_conf.knystd.vermont.fukujin.*;
import jp.gr.java_conf.knystd.vermont.util.*;


/*------------------------------------------------------------------------------ */
/* Player */
/* 敵キャラ */
/*------------------------------------------------------------------------------ */
public class Player extends GameCharacter {

    public static final int STATE_NORMAL = 0;
    public static final int STATE_DAMAGE = 1;
    public static final int STATE_ATTACK = 2;
    public static final int STATE_DEAD = 3;

    public static final int ANIM_TIME = 60;
    public static final int DAMAGE_TIME = 30;
    public static final int ATTACK_TIME = 10;
    public static final int DEAD_TIME = 120;

    public Player(double x, double y) {
        super(x, y, 100, 100, 3);
        this.m_state = Player.STATE_NORMAL;
        this.m_phase = Player.ANIM_TIME;
    }

    public int getState() {
        return this.m_state;
    }

    public void setState(int state) {
        this.m_state = state;
        if (state == Player.STATE_NORMAL) {
            this.setPhase(Player.ANIM_TIME);
        } else if (state == Player.STATE_DAMAGE) {
            this.setPhase(Player.DAMAGE_TIME);
        } else if (state == Player.STATE_ATTACK) {
            this.setPhase(Player.ATTACK_TIME);
        } else if (state == Player.STATE_DEAD) {
            this.setPhase(Player.DEAD_TIME);
        }
    }

    public void damage() {
        int hp = this.getHp();
        if (hp > 0) {
            this.setHp(hp - 1);
        }

        if (hp == 1) {
            this.setState(Player.STATE_DEAD);
        } else {
            this.setState(Player.STATE_DAMAGE);
        }
    }

    public void attack() {
        this.setState(Player.STATE_ATTACK);
    }

    @Override
    public void update(GameCore gameCore) {
        int phase = this.getPhase();
        if (phase > 0) {
            phase = phase - 1;
            this.setPhase(phase);
        }

        if (phase == 0) {
            int state = this.getState();
            if (state == Player.STATE_NORMAL) {
                /* アニメパターン繰り返し */
                this.setState(Player.STATE_NORMAL);
            } else if (state == Player.STATE_DAMAGE) {
                this.setState(Player.STATE_NORMAL);
            } else if (state == Player.STATE_ATTACK) {
                this.setState(Player.STATE_NORMAL);
            }
        }
    }

    @Override
    public void draw(VermontExecutor ex) {
        /* 仮の描画処理 */
        int x = this.getDispX();
        int y = this.getDispY();
        int sx = 0;
        int state = this.getState();
        if (state == Player.STATE_NORMAL || state == Player.STATE_DAMAGE) {
            if (this.m_phase < Player.ANIM_TIME / 2) {
                sx = 617;
            } else {
                sx = 719;
            }
        } else if (state == Player.STATE_ATTACK) {
            sx = 821;
        } else if (state == Player.STATE_DEAD) {
            sx = 923;
        }
        ex.blt(x, y, SampleFantasyApp.IMG_MAIN, sx, 67, 100, 100);


        this.drawHp(ex, x + 50, y + 110, this.getHp(), this.getMaxHp());
    }

    protected int m_state;
}
