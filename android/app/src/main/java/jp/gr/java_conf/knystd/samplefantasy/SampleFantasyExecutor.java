package jp.gr.java_conf.knystd.samplefantasy;
import android.app.Activity;

import java.util.ArrayList;
import java.util.Random;

import jp.gr.java_conf.knystd.vermont.*;
import jp.gr.java_conf.knystd.vermont.fukujin.*;
import jp.gr.java_conf.knystd.vermont.util.*;


/*------------------------------------------------------------------------------ */
/* SampleFantasyExecutor */
/* VermontのAPI実行を受け持つクラス */
/*------------------------------------------------------------------------------ */
public class SampleFantasyExecutor extends VermontExecutor {

    public SampleFantasyExecutor() {
        super();
        this.m_app = null;
    }

    public void onStart() {
        this.m_app = new SampleFantasyApp(this);
    }

    public void onLoad() {
    }

    public void onUpdate() {
        this.m_app.onUpdate();
    }

    public void onDraw() {
        this.m_app.onDraw();
    }

    public void onTouchEvent(int action, int count, int[] coord, int[] id) {
        this.m_app.onTouchEvent(action, count, coord, id);
    }

    public boolean onBackPressed() {
        return this.m_app.onBackPressed();
    }

    protected SampleFantasyApp m_app;
}
