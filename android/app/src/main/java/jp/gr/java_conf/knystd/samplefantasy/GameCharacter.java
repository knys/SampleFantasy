package jp.gr.java_conf.knystd.samplefantasy;
import android.app.Activity;

import java.util.ArrayList;
import java.util.Random;

import jp.gr.java_conf.knystd.vermont.*;
import jp.gr.java_conf.knystd.vermont.fukujin.*;
import jp.gr.java_conf.knystd.vermont.util.*;


/*------------------------------------------------------------------------------ */
/* GameCharacter */
/* ゲーム内のキャラクター */
/*------------------------------------------------------------------------------ */
public class GameCharacter extends GameObject {

    public GameCharacter(double x, double y, double width, double height, int hp) {
        super(x, y, width, height);
        this.m_hp = hp;
        this.m_maxHp = hp;
        this.m_phase = 0;
    }

    public int getHp() {
        return this.m_hp;
    }

    public void setHp(int hp) {
        this.m_hp = hp;
    }

    public int getMaxHp() {
        return this.m_maxHp;
    }

    public void setMaxHp(int maxHp) {
        this.m_maxHp = maxHp;
    }

    public int getPhase() {
        return this.m_phase;
    }

    public void setPhase(int phase) {
        this.m_phase = phase;
    }

    public void draw(VermontExecutor ex) {
    }

    public void update(GameCore gameCore) {
    }

    public void drawHp(VermontExecutor ex, int cx, int cy, int hp, int maxHp) {
        int dx = cx - maxHp * 10;
        int dy = cy - 10;
        for (int i = 0; i <= maxHp - 1; i++) {
            int sx;
            if (hp > i) {
                sx = 187;
            } else {
                sx = 165;
            }
            ex.blt(dx, dy, SampleFantasyApp.IMG_MAIN, sx, 1, 20, 20);
            dx = dx + 20;
        }
    }

    protected int m_hp;
    protected int m_maxHp;
    protected int m_phase;
}
