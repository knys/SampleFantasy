package jp.gr.java_conf.knystd.samplefantasy;
import android.app.Activity;

import java.util.ArrayList;
import java.util.Random;

import jp.gr.java_conf.knystd.vermont.*;
import jp.gr.java_conf.knystd.vermont.fukujin.*;
import jp.gr.java_conf.knystd.vermont.util.*;


/*------------------------------------------------------------------------------ */
/* Slime */
/* スライム */
/*------------------------------------------------------------------------------ */
public class Slime extends Enemy {

    public Slime(double x, double y, int number) {
        super(x, y, 80, 80, 1, number);
    }

    @Override
    public void draw(VermontExecutor ex) {
        int x = this.getDispX();
        int y = this.getDispY();
        this.drawEnemy(ex, x, y, 1, 1, 80, 80);
        this.drawNumber(ex, x + 40, y + 40, this.getNumber());
        this.drawHp(ex, x + 40, y + 90, this.getHp(), this.getMaxHp());
    }

}
