package jp.gr.java_conf.knystd.vermont;

import android.content.res.AssetFileDescriptor;
import android.content.res.Resources;
import android.content.res.Resources.NotFoundException;
import android.media.MediaPlayer;

import java.io.IOException;

class MusicManager {
    private class Music {
        private MediaPlayer m_mediaPlayer = null;
        private boolean m_autoPaused = false;

        MediaPlayer getMediaPlayer() {
            return m_mediaPlayer;
        }

        void setMediaPlayer(MediaPlayer mediaPlayer) {
            m_mediaPlayer = mediaPlayer;
        }

        boolean isAutoPaused() {
            return m_autoPaused;
        }

        void setAutoPaused(boolean autoPaused) {
            m_autoPaused = autoPaused;
        }
    }

    private Resources m_res;
    private Music[] m_music;
    private boolean m_enabled;

    MusicManager(Resources res, int musicMax) {
        m_res = res;
        m_music = new Music[musicMax];
        for (int i = 0; i < m_music.length; i++)
            m_music[i] = new Music();
        m_enabled = true;
    }

    void init() {
        for (Music music : m_music) {
            MediaPlayer mediaPlayer = music.getMediaPlayer();
            if (mediaPlayer != null) {
                mediaPlayer.release();
                music.setMediaPlayer(null);
            }
        }
    }

    void enable(boolean enabled) {
        m_enabled = enabled;
        if (!enabled) {
            stopAll();
        }
    }

    int load(int id, int index) {
        if (index < -1 || index >= m_music.length)
            return -1;

        // 空きを探す
        if (index < 0) {
            for (int i = 0; i < m_music.length; i++) {
                if (m_music[i].getMediaPlayer() == null) {
                    index = i;
                    break;
                }
            }
        }
        // 空きがない場合
        if (index < 0)
            return -1;

        AssetFileDescriptor afd = null;
        try {
            Music music = m_music[index];
            MediaPlayer mediaPlayer = music.getMediaPlayer();
            if (mediaPlayer == null) {
                mediaPlayer = new MediaPlayer();
            } else {
                mediaPlayer.stop();
                mediaPlayer.reset();
                music.setMediaPlayer(null);
            }
            afd = m_res.openRawResourceFd(id);
            mediaPlayer.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
            mediaPlayer.prepare();
            music.setMediaPlayer(mediaPlayer);
        } catch (NotFoundException e) {
            return -1;
        } catch (IllegalArgumentException e) {
            return -1;
        } catch (IllegalStateException e) {
            return -1;
        } catch (IOException e) {
            return -1;
        } finally {
            if (afd != null) {
                try {
                    afd.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return index;
    }

    void free(int index) {
        if (index < 0 || index >= m_music.length)
            return;

        Music music = m_music[index];
        MediaPlayer mediaPlayer = music.getMediaPlayer();
        if (mediaPlayer != null) {
            mediaPlayer.stop();
            mediaPlayer.reset();
            music.setMediaPlayer(null);
        }
    }

    void play(int index, boolean looping, float leftVolume, float rightVolume) {
        if (!m_enabled || index < 0 || index >= m_music.length)
            return;

        MediaPlayer mediaPlayer = m_music[index].getMediaPlayer();
        if (mediaPlayer != null) {
            mediaPlayer.setLooping(looping);
            mediaPlayer.setVolume(leftVolume, rightVolume);
            mediaPlayer.seekTo(0);
            if (!mediaPlayer.isPlaying()) {
                // 再生中でなければ再生する
                mediaPlayer.start();
            }
        }
    }

    void pause(int index) {
        if (!m_enabled || index < 0 || index >= m_music.length)
            return;

        MediaPlayer mediaPlayer = m_music[index].getMediaPlayer();
        if (mediaPlayer != null && mediaPlayer.isPlaying())
            mediaPlayer.pause();
    }

    void resume(int index) {
        if (!m_enabled || index < 0 || index >= m_music.length)
            return;

        MediaPlayer mediaPlayer = m_music[index].getMediaPlayer();
        if (mediaPlayer != null)
            mediaPlayer.start();
    }

    void stop(int index) {
        if (!m_enabled || index < 0 || index >= m_music.length)
            return;

        MediaPlayer mediaPlayer = m_music[index].getMediaPlayer();
        if (mediaPlayer != null && mediaPlayer.isPlaying()) {
            mediaPlayer.pause();
            mediaPlayer.seekTo(0);
        }
    }

    void setVolume(int index, float lvol, float rvol) {
        if (!m_enabled || index < 0 || index >= m_music.length)
            return;

        MediaPlayer mediaPlayer = m_music[index].getMediaPlayer();
        mediaPlayer.setVolume(lvol, rvol);
    }

    void stopAll() {
        if (m_music == null)
            return;

        for (Music music : m_music) {
            MediaPlayer mediaPlayer = music.getMediaPlayer();
            if (mediaPlayer != null && mediaPlayer.isPlaying()) {
                mediaPlayer.pause();
                mediaPlayer.seekTo(0);
            }
        }
    }

    void onPause() {
        if (m_music == null)
            return;

        for (Music music : m_music) {
            MediaPlayer mediaPlayer = music.getMediaPlayer();
            if (mediaPlayer != null && mediaPlayer.isPlaying()) {
                mediaPlayer.pause();
                music.setAutoPaused(true);
            }
        }
    }

    void onResume() {
        if (m_music == null)
            return;

        if (m_enabled) {
            for (Music music : m_music) {
                MediaPlayer mediaPlayer = music.getMediaPlayer();
                if (mediaPlayer != null && music.isAutoPaused()) {
                    mediaPlayer.start();
                    music.setAutoPaused(false);
                }
            }
        } else {
            stopAll();
            for (Music music : m_music) {
                music.setAutoPaused(false);
            }
        }
    }
}
