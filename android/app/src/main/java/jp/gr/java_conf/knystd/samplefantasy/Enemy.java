package jp.gr.java_conf.knystd.samplefantasy;
import android.app.Activity;

import java.util.ArrayList;
import java.util.Random;

import jp.gr.java_conf.knystd.vermont.*;
import jp.gr.java_conf.knystd.vermont.fukujin.*;
import jp.gr.java_conf.knystd.vermont.util.*;


/*------------------------------------------------------------------------------ */
/* Enemy */
/* 敵キャラ */
/*------------------------------------------------------------------------------ */
public class Enemy extends GameCharacter {

    public static final int STATE_NORMAL = 0;
    public static final int STATE_MOVE = 1;
    public static final int STATE_DAMAGE = 2;
    public static final int STATE_DEAD = 3;
    public static final int STATE_VANISHED = 4;

    public static final int ANIM_TIME = 60;
    public static final int DAMAGE_TIME = 30;
    public static final int DEAD_TIME = 40;
    public static final int MOVE_TIME = 60;

    public static final int MOVE_SPEED = 11;

    public Enemy(double x, double y, double width, double height, int hp, int number) {
        super(x, y - Enemy.MOVE_SPEED * Enemy.MOVE_TIME, width, height, hp);
        this.m_number = number;
        this.m_state = Enemy.STATE_MOVE;
        this.m_phase = Enemy.MOVE_TIME;
    }

    public int getNumber() {
        return this.m_number;
    }

    public void setNumber(int number) {
        this.m_number = number;
    }

    public int getState() {
        return this.m_state;
    }

    public void setState(int state) {
        this.m_state = state;
        if (state == Enemy.STATE_NORMAL) {
            this.setPhase(Enemy.ANIM_TIME);
        } else if (state == Enemy.STATE_DAMAGE) {
            this.setPhase(Enemy.DAMAGE_TIME);
        } else if (state == Enemy.STATE_DEAD) {
            this.setPhase(Enemy.DEAD_TIME);
        }
    }

    @Override
    public void update(GameCore gameCore) {
        int phase = this.getPhase();
        if (phase > 0) {
            phase = phase - 1;
            this.setPhase(phase);

            int state = this.getState();
            if (state == Enemy.STATE_MOVE) {
                double y = this.getY();
                y = y + Enemy.MOVE_SPEED;
                this.setY(y);
            }

            if (phase == 0) {
                if (state == Enemy.STATE_NORMAL) {
                    /* アニメパターン繰り返し */
                    this.setState(Enemy.STATE_NORMAL);
                } else if (state == Enemy.STATE_MOVE) {
                    this.setState(Enemy.STATE_NORMAL);
                } else if (state == Enemy.STATE_DAMAGE) {
                    this.setState(Enemy.STATE_NORMAL);
                } else if (state == Enemy.STATE_DEAD) {
                    this.setState(Enemy.STATE_VANISHED);
                }
            }
        }
    }

    public void drawEnemy(VermontExecutor ex, int dx, int dy, int sx, int sy, int sw, int sh) {
        int state = this.getState();
        if (state == Enemy.STATE_NORMAL || state == Enemy.STATE_MOVE) {
            int sx1;
            if (this.m_phase < Enemy.ANIM_TIME / 2) {
                sx1 = sx;
            } else {
                sx1 = sx + sw + 2;
            }
            ex.blt(dx, dy, SampleFantasyApp.IMG_MAIN, sx1, sy, sw, sh);
        } else if (state == Enemy.STATE_DAMAGE) {
            ex.blt(dx, dy, SampleFantasyApp.IMG_MAIN, sx, sy, sw, sh);

            int cm = ex.selectColormap(SampleFantasyApp.CM_DAMAGE);
            int bm = ex.setBlendMode(VermontExecutor.BLEND_NORMAL);
            int alpha = ex.setAlpha((int)(255 * this.m_phase / Enemy.DAMAGE_TIME));
            ex.blt(dx, dy, SampleFantasyApp.IMG_MAIN, sx, sy, sw, sh);
            ex.setAlpha(alpha);
            ex.setBlendMode(bm);
            ex.selectColormap(cm);
        } else if (state == Enemy.STATE_DEAD) {
            int cm = ex.selectColormap(SampleFantasyApp.CM_DAMAGE);
            int bm = ex.setBlendMode(VermontExecutor.BLEND_ADD);
            int alpha = ex.setAlpha((int)(255 * this.m_phase / Enemy.DEAD_TIME));
            ex.blt(dx, dy, SampleFantasyApp.IMG_MAIN, sx, sy, sw, sh);
            ex.setAlpha(alpha);
            ex.setBlendMode(bm);
            ex.selectColormap(cm);
        }
    }

    public void drawNumber(VermontExecutor ex, int cx, int cy, int number) {
        int state = this.getState();
        if (state != Enemy.STATE_NORMAL && state != Enemy.STATE_DAMAGE) {
            return;
        }

        int dx = cx - 20;
        int dy = cy - 32;
        if (number >= 10) {
            dx = dx + 20;
        }

        while (number > 0) {
            int number1 = number % 10;
            number = (int)(number / 10);
            int sx = 605 + number1 * 42;
            ex.blt(dx, dy, SampleFantasyApp.IMG_MAIN, sx, 1, 40, 64);
            dx = dx - 40;
        }
    }

    public void hit() {
        int hp = this.getHp();
        if (hp > 0) {
            this.setHp(hp - 1);
        }

        if (hp == 1) {
            this.setState(Enemy.STATE_DEAD);
        } else {
            this.setState(Enemy.STATE_DAMAGE);
        }
    }

    protected int m_number;
    protected int m_state;
}
