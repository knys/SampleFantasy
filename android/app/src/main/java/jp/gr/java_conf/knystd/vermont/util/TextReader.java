package jp.gr.java_conf.knystd.vermont.util;

import android.content.Context;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class TextReader {
    public static final int MODE_PRIVATE = 0;
    public static final int MODE_EXTERNAL = 1;
    public static final int MODE_ASSET = 2;

    public TextReader() {
        this.m_br = null;
    }

    public boolean open(int mode, Context context, String filename) {
        try {
            switch (mode) {
                case MODE_PRIVATE:
                    FileInputStream fis = context.openFileInput(filename);
                    m_br = new BufferedReader(new InputStreamReader(fis));
                    break;
                case MODE_EXTERNAL:
                    File file = new File(context.getExternalFilesDir(null), filename);
                    m_br = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
                    break;
                case MODE_ASSET:
                    InputStream is = context.getAssets().open(filename);
                    m_br = new BufferedReader(new InputStreamReader(is));
                    break;
            }
        } catch (IOException e) {
            m_br = null;
            return false;
        }

        return true;
    }

    public void close() {
        if (this.m_br != null) {
            try {
                this.m_br.close();
            } catch (IOException e) {
            }
            this.m_br = null;
        }
    }

    public String readLine() {
        try {
            return m_br.readLine();
        } catch (IOException e) {
            return null;
        }
    }

    protected BufferedReader m_br;
}
