package jp.gr.java_conf.knystd.samplefantasy;
import android.app.Activity;

import java.util.ArrayList;
import java.util.Random;

import jp.gr.java_conf.knystd.vermont.*;
import jp.gr.java_conf.knystd.vermont.fukujin.*;
import jp.gr.java_conf.knystd.vermont.util.*;


/*------------------------------------------------------------------------------ */
/* StageData */
/* ステージの敵配置 */
/*------------------------------------------------------------------------------ */
public class StageData {

    public StageData() {
        this.m_data = new ArrayList();
    }

    public ArrayList<ArrayList<EnemyData>> getData() {
        return this.m_data;
    }

    public void load(Activity activity, String filename) {
        this.m_data.clear();
        ArrayList<EnemyData> list = new ArrayList();
        this.m_data.add(list);

        TextReader tr = new TextReader();
        if (tr.open(TextReader.MODE_ASSET, activity, filename)) {
            String s = tr.readLine();
            while (s != null) {
                if (s.equals("")) {
                    list = new ArrayList();
                    this.m_data.add(list);
                } else {
                    String[] arr = s.split(",", 0);
                    int enemyType = Integer.parseInt(arr[0]);
                    int x = Integer.parseInt(arr[1]);
                    int y = Integer.parseInt(arr[2]);
                    EnemyData enemyData = new EnemyData(enemyType, x, y);
                    list.add(enemyData);
                }
                s = tr.readLine();
            }
            tr.close();
        }
    }

    protected ArrayList<ArrayList<EnemyData>> m_data;
}
