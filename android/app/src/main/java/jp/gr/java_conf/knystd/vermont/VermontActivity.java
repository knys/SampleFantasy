package jp.gr.java_conf.knystd.vermont;

import android.app.Activity;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.opengl.GLSurfaceView;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.WindowManager;

public class VermontActivity extends Activity implements SensorEventListener {
    protected GLSurfaceView m_view;
    protected VermontRenderer m_renderer;
    protected VermontExecutor m_executor;

    protected SensorManager m_sensorManager = null;
    protected Sensor m_orientationSensor = null;
    protected float m_orientation[] = new float[3];
    protected Sensor m_accelSensor = null;
    protected float m_accel[] = new float[3];
    protected int[] m_location = new int[2];

    private static final int TEXTURE_MAX = 32;

    /* onCreateで呼ぶ */
    protected void init(VermontExecutor ex, boolean enableSensor, boolean enableFrameSkip) {
        m_executor = ex;
        m_renderer = new VermontRenderer(getResources(), ex, TEXTURE_MAX);
        m_view = new GLSurfaceView(this);
        m_view.setEGLContextClientVersion(2);
        m_view.setEGLConfigChooser(5, 6, 5, 0, 0, 0);
        m_view.setRenderer(m_renderer);
        m_view.setKeepScreenOn(true);

        if (enableSensor) {
            m_sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
            m_orientationSensor = null;
            m_accelSensor = null;
            for (int i = 0; i < 3; i++) {
                m_orientation[i] = 0.0f;
                m_accel[i] = 0.0f;
            }
        }
        Resources res = getResources();
        m_executor.init(this, m_renderer, res);
        setContentView(m_view);
        reset();

        enableFrameSkip(enableFrameSkip);
        ex.enableSound(true);
        ex.onStart();
        ex.onLoad();
    }

    // 処理が重いときにフレームスキップするかどうか
    protected void enableFrameSkip(boolean b) {
        m_renderer.enableFrameSkip(b);
    }

    // 負荷メーターを表示するかどうか
    protected void showLoad(boolean b) {
        m_renderer.showLoad(b);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private void reset() {
        m_executor.reset();
        m_renderer.init();
    }

    @Override
    protected void onPause() {
        super.onPause();
        m_view.onPause();
        m_executor.onPause();
        if (m_orientationSensor != null || m_accelSensor != null) {
            m_sensorManager.unregisterListener(this);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        m_view.onResume();
        m_executor.onResume();
        if (m_sensorManager != null) {
            m_orientationSensor = m_sensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION);
            if (m_orientationSensor != null)
                m_sensorManager.registerListener(this, m_orientationSensor, SensorManager.SENSOR_DELAY_GAME);
            m_accelSensor = m_sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
            if (m_accelSensor != null)
                m_sensorManager.registerListener(this, m_accelSensor, SensorManager.SENSOR_DELAY_GAME);
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int action = event.getAction();
        int vtAction = VermontExecutor.ACTION_OTHER;
        switch (action & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_DOWN:
                vtAction = VermontExecutor.ACTION_DOWN;
                break;
            case MotionEvent.ACTION_MOVE:
                vtAction = VermontExecutor.ACTION_MOVE;
                break;
            case MotionEvent.ACTION_UP:
                vtAction = VermontExecutor.ACTION_UP;
                break;
            case MotionEvent.ACTION_POINTER_DOWN:
                vtAction = VermontExecutor.ACTION_POINTER_DOWN;
                break;
            case MotionEvent.ACTION_POINTER_UP:
                vtAction = VermontExecutor.ACTION_POINTER_UP;
                break;
        }

        if (vtAction == VermontExecutor.ACTION_OTHER) {
            return super.onTouchEvent(event);
        } else {
            int count = event.getPointerCount();
            int[] coord = new int[count * 2];
            int[] id = new int[count];
            if (vtAction == VermontExecutor.ACTION_POINTER_DOWN || vtAction == VermontExecutor.ACTION_POINTER_UP) {
                int actPtrIndex = (action & MotionEvent.ACTION_POINTER_ID_MASK) >> MotionEvent.ACTION_POINTER_ID_SHIFT;
                int index = count - 1;
                for (int i = 0; i < count; i++) {
                    int x1 = (int) event.getX(i);
                    int y1 = (int) event.getY(i);
                    int id1 = event.getPointerId(i);
                    if (i == actPtrIndex) {
                        // ACTION_POINTER_DOWN/UPで変化したIDを先頭に
                        coord[0] = x1;
                        coord[1] = y1;
                        id[0] = id1;
                    } else {
                        // その他は後ろから
                        coord[index * 2] = x1;
                        coord[index * 2 + 1] = y1;
                        id[index] = id1;
                        index--;
                    }
                }
            } else {
                for (int i = 0; i < count; i++) {
                    coord[i * 2] = (int) event.getX(i);
                    coord[i * 2 + 1] = (int) event.getY(i);
                    id[i] = event.getPointerId(i);
                }
            }

            // 座標変換
            m_view.getLocationOnScreen(m_location);
            for (int i = 0; i < count; i++) {
                coord[i * 2] -= m_location[0];
                coord[i * 2 + 1] -= m_location[1];
            }
            m_renderer.deviceToLogical(coord, count);

            m_executor.postTouchEvent(vtAction, count, coord, id);
            return true;
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        int rotation = ((WindowManager) getSystemService(WINDOW_SERVICE)).getDefaultDisplay().getRotation();

        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            switch (rotation) {
                case Surface.ROTATION_0:
                    m_accel[0] = event.values[0];
                    m_accel[1] = event.values[1];
                    break;
                case Surface.ROTATION_90:
                    m_accel[0] = -event.values[1];
                    m_accel[1] = event.values[0];
                    break;
                case Surface.ROTATION_180:
                    m_accel[0] = -event.values[0];
                    m_accel[1] = -event.values[1];
                    break;
                case Surface.ROTATION_270:
                    m_accel[0] = event.values[1];
                    m_accel[1] = -event.values[0];
                    break;
            }
            m_accel[2] = event.values[2];
        } else if (event.sensor.getType() == Sensor.TYPE_ORIENTATION) {
            switch (rotation) {
                case Surface.ROTATION_0:
                    m_orientation[0] = event.values[0];
                    m_orientation[1] = event.values[1];
                    m_orientation[2] = event.values[2];
                    break;
                case Surface.ROTATION_90:
                    m_orientation[0] = event.values[0] + 90;
                    m_orientation[1] = event.values[2];
                    m_orientation[2] = -event.values[1];
                    break;
                case Surface.ROTATION_180:
                    m_orientation[0] = event.values[0] + 180;
                    m_orientation[1] = -event.values[1];
                    m_orientation[2] = -event.values[2];
                    break;
                case Surface.ROTATION_270:
                    m_orientation[0] = event.values[0] + 270;
                    m_orientation[1] = -event.values[2];
                    m_orientation[2] = event.values[1];
                    break;
            }
            if (m_orientation[0] > 360)
                m_orientation[0] -= 360;
        }
    }

    public float getOrientation(int index) {
        return m_orientation[index];
    }

    public float getAccel(int index) {
        return m_accel[index];
    }

    @Override
    public void onBackPressed() {
        m_executor.postBackPressed();
    }
}
