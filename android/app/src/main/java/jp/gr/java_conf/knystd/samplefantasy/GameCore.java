package jp.gr.java_conf.knystd.samplefantasy;
import android.app.Activity;

import java.util.ArrayList;
import java.util.Random;

import jp.gr.java_conf.knystd.vermont.*;
import jp.gr.java_conf.knystd.vermont.fukujin.*;
import jp.gr.java_conf.knystd.vermont.util.*;


/*------------------------------------------------------------------------------ */
/* GameCore */
/* ゲームのメインデータ */
/*------------------------------------------------------------------------------ */
public class GameCore extends GameObject {

    public static final int MODE_MOVE = 0;
    public static final int MODE_NORMAL = 1;
    public static final int MODE_GAMEOVER = 2;
    public static final int MODE_CLEAR = 3;
    public static final int MODE_END = 4;

    public static final int GAMEOVER_TIME = 120;
    public static final int CLEAR_TIME = 120;

    public GameCore(VermontExecutor ex) {
        super(SampleFantasyApp.WIN_WIDTH - 16, 0, 16, 16);
        this.m_ex = ex;
        this.m_enemies = new ArrayList();
        this.m_player = new Player(190, 550);
        this.m_number = 0;
        this.m_stageData = new StageData();
        this.m_mode = 0;
        this.m_phase = 0;
        this.m_wave = 0;
        this.m_time = 0;
        this.m_random = new Random();
        this.m_existEnemy = true;
        this.m_isCleared = false;
    }

    public int getMode() {
        return this.m_mode;
    }

    public int getTime() {
        return this.m_time;
    }

    public boolean isCleared() {
        return this.m_isCleared;
    }

    public void setMode(int mode) {
        this.m_mode = mode;

        /* 時間が決められているものは設定する */
        if (mode == GameCore.MODE_MOVE) {
            this.m_phase = Enemy.MOVE_TIME;
        } else if (mode == GameCore.MODE_GAMEOVER) {
            this.m_phase = GameCore.GAMEOVER_TIME;
        } else if (mode == GameCore.MODE_CLEAR) {
            this.m_phase = GameCore.CLEAR_TIME;
        }
    }

    public int getPhase() {
        return this.m_phase;
    }

    public void init(int level) {
        this.m_wave = 0;
        this.m_time = 0;

        Activity activity = this.m_ex.getActivity();
        String filename = "level" + level + ".dat";
        this.m_stageData.load(activity, filename);
        this.setEnemy();
    }

    public void close() {
    }

    public void setEnemy() {
        ArrayList<ArrayList<EnemyData>> data = this.m_stageData.getData();
        if (data.size() <= this.m_wave) {
            this.m_isCleared = true;
            this.setMode(GameCore.MODE_CLEAR);
            return;
        }
        ArrayList<EnemyData> list = data.get(this.m_wave);

        int number = 1;

        for (int i = 0; i <= list.size() - 1; i++) {
            EnemyData enemyData = list.get(i);
            int enemyType = enemyData.getEnemyType();
            int x = enemyData.getX();
            int y = enemyData.getY();

            if (enemyType == 1) {
                this.m_enemies.add(new Slime(x, y, number));
            } else if (enemyType == 2) {
                this.m_enemies.add(new Snake(x, y, number));
            } else if (enemyType == 3) {
                this.m_enemies.add(new Orc(x, y, number));
            } else if (enemyType == 4) {
                this.m_enemies.add(new Griffin(x, y, number));
            } else if (enemyType == 5) {
                this.m_enemies.add(new Golem(x, y, number));
            } else {
                this.m_enemies.add(new Dragon(x, y, number));
            }

            number = number + 1;
        }

        this.shuffle();

        this.m_number = 1;
        this.m_existEnemy = true;

        this.setMode(GameCore.MODE_MOVE);
    }

    public void shuffle() {
        int size = this.m_enemies.size();
        ArrayList<ArrayList<EnemyData>> data = this.m_stageData.getData();
        if (data.size() - 1 == this.m_wave) {
            size = size - 1;
        }

        for (int i = 0; i <= size - 1; i++) {
            int index = this.m_random.nextInt(size);
            Enemy enemy1 = this.m_enemies.get(i);
            Enemy enemy2 = this.m_enemies.get(index);
            int number1 = enemy1.getNumber();
            int number2 = enemy2.getNumber();
            enemy2.setNumber(number1);
            enemy1.setNumber(number2);
        }
    }

    public void update() {
        if (this.m_mode == GameCore.MODE_NORMAL) {
            this.m_time = this.m_time + 1;
        }

        if (this.m_phase > 0) {
            this.m_phase = this.m_phase - 1;
            if (this.m_phase == 0) {
                if (this.m_mode == GameCore.MODE_MOVE) {
                    this.m_mode = GameCore.MODE_NORMAL;
                } else if (this.m_mode == GameCore.MODE_GAMEOVER || this.m_mode == GameCore.MODE_CLEAR) {
                    this.m_mode = GameCore.MODE_END;
                }
            }
        }

        for (int i = 0; i <= this.m_enemies.size() - 1; i++) {
            Enemy enemy = this.m_enemies.get(i);
            enemy.update(this);
        }

        /* 消滅したキャラを開放 */
        this.m_existEnemy = false;
        for (int i = this.m_enemies.size() - 1; i >= 0; i -= 1) {
            Enemy enemy = this.m_enemies.get(i);
            int state = enemy.getState();
            if (state == Enemy.STATE_VANISHED) {
                this.m_enemies.remove(i);
                if (this.m_enemies.size() == 0) {
                    this.m_wave = this.m_wave + 1;
                    this.setEnemy();
                }
            } else if (state == Enemy.STATE_NORMAL || state == Enemy.STATE_DAMAGE) {
                this.m_existEnemy = true;
            }
        }

        this.m_player.update(this);
    }

    public void draw() {
        VermontExecutor ex = this.m_ex;

        int oy = 0;
        if (this.m_mode == GameCore.MODE_MOVE) {
            oy = (this.m_phase * Enemy.MOVE_SPEED) % 350;
        }
        ex.tileBlt(0, 0, SampleFantasyApp.WIN_WIDTH, SampleFantasyApp.WIN_HEIGHT, SampleFantasyApp.IMG_MAIN, 543, 170, 480, 350, 0, oy);

        /* 被ダメージエフェクト */
        int alpha = 0;
        if (this.m_player.getState() == Player.STATE_DAMAGE) {
            int phase = this.m_player.getPhase();
            alpha = (int)(256 * phase / Player.DAMAGE_TIME);
        }
        if (this.m_player.getState() == Player.STATE_DEAD) {
            int phase = this.m_player.getPhase();
            alpha = (int)(256 * phase / Player.DEAD_TIME);
        }
        if (alpha > 0) {
            int color = ex.setColor(ex.argb(alpha, 255, 0, 0));
            ex.fillRect(0, 0, SampleFantasyApp.WIN_WIDTH, SampleFantasyApp.WIN_HEIGHT);
            ex.setColor(color);
        }

        for (int i = 0; i <= this.m_enemies.size() - 1; i++) {
            Enemy enemy = this.m_enemies.get(i);
            enemy.draw(ex);
        }
        this.m_player.draw(ex);

        DrawUtil.drawTime(ex, 360, 0, this.m_time);

        if (this.m_mode == GameCore.MODE_GAMEOVER) {
            ex.blt(0, 250, SampleFantasyApp.IMG_MAIN, 543, 754, 480, 128);
        } else if (this.m_mode == GameCore.MODE_CLEAR) {
            ex.blt(0, 250, SampleFantasyApp.IMG_MAIN, 543, 624, 480, 128);
        }
    }

    public void attack(int x, int y) {
        if (this.getMode() != GameCore.MODE_NORMAL || ! this.m_existEnemy) {
            return;
        }

        boolean isHit = false;

        for (int i = 0; i <= this.m_enemies.size() - 1; i++) {
            Enemy enemy = this.m_enemies.get(i);
            int dx = enemy.getDispX();
            int dy = enemy.getDispY();
            int dw = enemy.getDispWidth();
            int dh = enemy.getDispHeight();
            if (x >= dx && x < dx + dw && y >= dy && y < dy + dh) {
                int state = enemy.getState();
                if ((state == Enemy.STATE_NORMAL || state == Enemy.STATE_DAMAGE) && this.m_number == enemy.getNumber()) {
                    this.m_player.attack();
                    enemy.hit();
                    if (enemy.getHp() == 0) {
                        this.m_number = this.m_number + 1;
                    }
                    isHit = true;
                    break;
                }
            }
        }

        /* 外したらダメージ */
        if (! isHit) {
            int state = this.m_player.getState();
            if (state == Player.STATE_NORMAL || state == Player.STATE_ATTACK) {
                this.m_player.damage();

                if (this.m_player.getHp() == 0) {
                    this.setMode(GameCore.MODE_GAMEOVER);
                }
            }
        }
    }

    protected VermontExecutor m_ex;
    protected ArrayList<Enemy> m_enemies;
    protected Player m_player;
    protected int m_number;
    protected StageData m_stageData;
    protected int m_mode;
    protected int m_phase;
    protected int m_wave;
    protected int m_time;
    protected Random m_random;
    protected boolean m_existEnemy;
    protected boolean m_isCleared;
}
