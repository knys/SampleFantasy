package jp.gr.java_conf.knystd.vermont;

import android.os.SystemClock;

class Timer {
    private int m_fps;
    private long m_time;
    private int m_elapsedTime; // フレーム開始からの時間(1000で1フレーム分)

    Timer(int fps) {
        setFps(fps);
    }

    void setFps(int fps) {
        m_fps = fps;
        m_time = SystemClock.elapsedRealtime();
        m_elapsedTime = 1000;
    }

    int getElapsedTime() {
        return m_elapsedTime;
    }

    int wait(int limit) {
        long prevTime = m_time;
        m_time = SystemClock.elapsedRealtime();
        // 経過時間を換算し、1フレーム分引くことで、現在のフレームの開始からの時間を求める
        m_elapsedTime += (m_time - prevTime) * m_fps - 1000;

        int skip;
        int frames = m_elapsedTime / 1000;
        if (frames > limit) {
            // 処理に要したフレーム数がlimitを超えたら、その分スキップして処理落ちさせる
            skip = frames - limit;
            m_elapsedTime -= skip * 1000;
        } else {
            // limit以内ならそのまま続行し、今後の追い上げに期待する
            skip = 0;
        }

        // 次のフレームまでsleep
        if (m_elapsedTime < 1000) {
            long time = (1000 - m_elapsedTime + m_fps - 1) / m_fps;
            if (time > 1) {
                try {
                    Thread.sleep(time);
                } catch (InterruptedException e) {
                }
            }
        }
        return skip;
    }
}
