package jp.gr.java_conf.knystd.vermont;

import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Environment;

import java.util.LinkedList;

public abstract class VermontExecutor {
    public static final int BLEND_NONE = 0;
    public static final int BLEND_NORMAL = 1;
    public static final int BLEND_ADD = 2;

    public static final int OP_REPLACE = 0;
    public static final int OP_MODULATE = 1;

    public static final int HALIGN_LEFT = 0;
    public static final int HALIGN_CENTER = 1;
    public static final int HALIGN_RIGHT = 2;

    public static final int VALIGN_TOP = 0;
    public static final int VALIGN_MIDDLE = 1;
    public static final int VALIGN_BOTTOM = 2;

    public static final int PLATFORM_WINDOWS = 0;
    public static final int PLATFORM_ANDROID = 1;

    public static final int ACTION_OTHER = -1;
    public static final int ACTION_DOWN = 0;
    public static final int ACTION_MOVE = 1;
    public static final int ACTION_UP = 2;
    public static final int ACTION_POINTER_DOWN = 3;
    public static final int ACTION_POINTER_UP = 4;

    private static final int SOUND_MAX = 16;
    private static final int PLAY_MAX = 8;
    private static final int MUSIC_MAX = 4;
    private static final int PATCH_MAX = 16;
    private static final int COLORMAP_MAX = 16;

    private VermontActivity m_activity;
    private VermontRenderer m_vtRenderer;
    private SoundManager m_soundManager;
    private MusicManager m_musicManager;
    private PatchManager m_patchManager;
    private ColormapManager m_colormapManager;
    private int m_colormapIndex;

    private Timer m_timer;
    private String[] m_errorMessages = null;
    private int m_width;
    private int m_height;
    private int[] m_size;
    private int[] m_buf;

    private class TouchEvent {
        private int m_action;
        private int m_count;
        private int[] m_coord;
        private int[] m_id;

        TouchEvent(int action, int count, int[] coord, int[] id) {
            m_action = action;
            m_count = count;
            m_coord = coord;
            m_id = id;
        }

        int getAction() {
            return m_action;
        }

        int getCount() {
            return m_count;
        }

        int[] getCoord() {
            return m_coord;
        }

        int[] getId() {
            return m_id;
        }
    }

    private final LinkedList<TouchEvent> m_touchEventList;
    private boolean m_backPressed;

    public VermontExecutor() {
        m_touchEventList = new LinkedList<>();
    }

    void init(VermontActivity activity, VermontRenderer vtRenderer, Resources res) {
        m_activity = activity;
        m_vtRenderer = vtRenderer;
        m_soundManager = new SoundManager(res, SOUND_MAX, PLAY_MAX);
        m_musicManager = new MusicManager(res, MUSIC_MAX);
        m_patchManager = new PatchManager(PATCH_MAX);
        m_colormapManager = new ColormapManager(COLORMAP_MAX);
    }

    protected int getWidth() {
        return m_width;
    }

    protected int getHeight() {
        return m_height;
    }

    ColormapManager.Colormap getColormap() {
        return m_colormapManager.getColormap(m_colormapIndex);
    }

    void reset() {
        m_timer = new Timer(30);
        m_errorMessages = null;
        m_vtRenderer.setColor(Color.WHITE);
        m_vtRenderer.setFontSize(16);
        m_vtRenderer.setTextHAlign(HALIGN_LEFT);
        m_vtRenderer.setTextVAlign(VALIGN_TOP);
        m_vtRenderer.setBlendMode(BLEND_NONE);
        m_vtRenderer.setTextureOperation(OP_REPLACE);
        m_vtRenderer.setAlpha(255);
        m_vtRenderer.clearMask();
        m_vtRenderer.initTextCache();
        m_musicManager.init();
        m_patchManager.init();
        m_colormapManager.init();
        m_colormapIndex = -1;
        m_width = 480;
        m_height = 800;
        m_size = new int[2];
        m_buf = new int[8];
        m_activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        m_touchEventList.clear();
        m_backPressed = false;
    }

    public void setResolution(int width, int height) {
        if (width <= 0 || height <= 0)
            return;
        m_width = width;
        m_height = height;
        if (m_width > m_height)
            m_activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        else
            m_activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    public void setFps(int fps) {
        m_timer.setFps(fps);
    }

    public int getLoad() {
        return m_timer.getElapsedTime();
    }

    public int loadImage(int id) {
        return loadImage(id, -1);
    }

    public int loadImage(int id, int index) {
        return m_vtRenderer.loadImage(id, index);
    }

    public void freeImage(int index) {
        m_vtRenderer.freeImage(index);
    }

    public int[] getImageSize(int img) {
        m_vtRenderer.getImageSize(img, m_size);
        return m_size;
    }

    public int rgb(int r, int g, int b) {
        return 0xff000000 + (r << 16) + (g << 8) + b;
    }

    public int argb(int a, int r, int g, int b) {
        return (a << 24) + (r << 16) + (g << 8) + b;
    }

    public int setColor(int color) {
        return m_vtRenderer.setColor(color);
    }

    public int setBlendMode(int blendMode) {
        return m_vtRenderer.setBlendMode(blendMode);
    }

    public int setTextureOperation(int operation) {
        return m_vtRenderer.setTextureOperation(operation);
    }

    public int setAlpha(int alpha) {
        return m_vtRenderer.setAlpha(alpha);
    }

    public int createColormap(int count) {
        return m_colormapManager.createColormap(count, -1);
    }

    public int createColormap(int count, int index) {
        return m_colormapManager.createColormap(count, index);
    }

    public void freeColormap(int index) {
        m_colormapManager.freeColormap(index);
    }

    public void setColormapValue(int index, int position, int border, int color) {
        ColormapManager.Colormap colormap = m_colormapManager.getColormap(index);
        if (colormap != null) {
            colormap.setValue(position, border, color);
        }
    }

    public int setColormapOffset(int index, int offset) {
        ColormapManager.Colormap colormap = m_colormapManager.getColormap(index);
        int oldOffset = -1;
        if (colormap != null) {
            oldOffset = colormap.getOffset();
            colormap.setOffset(offset);
        }
        return oldOffset;
    }

    public int selectColormap(int index) {
        int oldIndex = m_colormapIndex;
        m_colormapIndex = index;
        return oldIndex;
    }

    public void setMask(int dx, int dy, int img, int sx, int sy, int sw, int sh) {
        m_vtRenderer.getImageSize(img, m_size);
        setMask(dx, dy, m_size[0], m_size[1], img, sx, sy, sw, sh);
    }

    public void setMask(int dx, int dy, int dw, int dh, int img, int sx, int sy, int sw, int sh) {
        m_vtRenderer.setMask(dx, dy, dw, dh, img, sx, sy, sw, sh);
    }

    public void clearMask() {
        m_vtRenderer.clearMask();
    }

    public void cls() {
        cls(0);
    }

    public void cls(int color) {
        m_vtRenderer.cls(color);
    }

    public int setFontSize(int size) {
        return m_vtRenderer.setFontSize(size);
    }

    public int setTextHAlign(int halign) {
        return m_vtRenderer.setTextHAlign(halign);
    }

    public int setTextVAlign(int valign) {
        return m_vtRenderer.setTextVAlign(valign);
    }

    public void drawText(int x, int y, String text) {
        m_vtRenderer.drawText(x, y, text);
    }

    public void drawLine(int x1, int y1, int x2, int y2) {
        m_vtRenderer.drawLine(x1, y1, x2, y2);
    }

    public void drawRect(int x, int y, int w, int h) {
        m_vtRenderer.drawRect(x, y, w, h);
    }

    public void fillRect(int x, int y, int w, int h) {
        m_vtRenderer.fillRect(x, y, w, h);
    }

    public void drawEllipse(int x, int y, int w, int h) {
        m_vtRenderer.drawEllipse(x, y, w, h);
    }

    public void fillEllipse(int x, int y, int w, int h) {
        m_vtRenderer.fillEllipse(x, y, w, h);
    }

    public void drawPolygon(int[] vertices) {
        m_vtRenderer.drawPolygon(vertices);
    }

    public void fillPolygon(int[] vertices) {
        m_vtRenderer.fillPolygon(vertices);
    }

    public void texturePolygon(int[] vertices, int img, int[] coords) {
        m_vtRenderer.texturePolygon(vertices, img, coords);
    }

    public void blt(int dx, int dy, int img) {
        m_vtRenderer.getImageSize(img, m_size);
        blt(dx, dy, img, 0, 0, m_size[0], m_size[1]);
    }

    public void blt(int dx, int dy, int img, int sx, int sy, int sw, int sh) {
        m_vtRenderer.blt(dx, dy, img, sx, sy, sw, sh);
    }

    public void stretchBlt(int dx, int dy, int dw, int dh, int img) {
        m_vtRenderer.getImageSize(img, m_size);
        stretchBlt(dx, dy, dw, dh, img, 0, 0, m_size[0], m_size[1]);
    }

    public void stretchBlt(int dx, int dy, int dw, int dh, int img, int sx, int sy, int sw, int sh) {
        m_vtRenderer.stretchBlt(dx, dy, dw, dh, img, sx, sy, sw, sh);
    }

    public void tileBlt(int dx, int dy, int dw, int dh, int img, int sx, int sy, int sw, int sh) {
        m_vtRenderer.tileBlt(dx, dy, dw, dh, img, sx, sy, sw, sh, 0, 0);
    }

    public void tileBlt(int dx, int dy, int dw, int dh, int img, int sx, int sy, int sw, int sh, int ox, int oy) {
        m_vtRenderer.tileBlt(dx, dy, dw, dh, img, sx, sy, sw, sh, ox, oy);
    }

    public void rotateBlt(int[] vertices, int img) {
        m_vtRenderer.getImageSize(img, m_size);
        rotateBlt(vertices, img, 0, 0, m_size[0], m_size[1]);
    }

    public void rotateBlt(int[] vertices, int img, int sx, int sy, int sw, int sh) {
        for (int i = 0; i < 6; i++)
            m_buf[i] = vertices[i];
        m_buf[6] = vertices[2] + vertices[4] - vertices[0];
        m_buf[7] = vertices[3] + vertices[5] - vertices[1];
        m_vtRenderer.rotateBlt(m_buf, img, sx, sy, sw, sh);
    }

    public int createPatch(int cols, int rows) {
        return m_patchManager.createPatch(cols, rows, -1);
    }

    public int createPatch(int cols, int rows, int index) {
        return m_patchManager.createPatch(cols, rows, index);
    }

    public void freePatch(int index) {
        m_patchManager.freePatch(index);
    }

    public void clearPatch(int index) {
        m_patchManager.clearPatch(index, (short) -1);
    }

    public void clearPatch(int index, short piece) {
        m_patchManager.clearPatch(index, piece);
    }

    public void setPiece(int index, int col, int row, short piece) {
        m_patchManager.setPiece(index, col, row, piece);
    }

    public short getPiece(int index, int col, int row) {
        return m_patchManager.getPiece(index, col, row);
    }

    public void patchBlt(int dx, int dy, int pat, int img, int sx, int sy, int pw, int ph, int scols, int srows) {
        PatchManager.Patch patch = m_patchManager.getPatch(pat);
        m_vtRenderer.patchBlt(dx, dy, patch, img, sx, sy, pw, ph, scols, srows);
    }

    public void patchStretchBlt(int dx, int dy, int dw, int dh, int pat, int img, int sx, int sy, int pw, int ph, int scols, int srows) {
        PatchManager.Patch patch = m_patchManager.getPatch(pat);
        m_vtRenderer.patchStretchBlt(dx, dy, dw, dh, patch, img, sx, sy, pw, ph, scols, srows);
    }

    public void initSound() {
        m_soundManager.init();
    }

    public void releaseSound() {
        m_soundManager.release();
    }

    public int loadSound(int id) {
        return m_soundManager.load(id, 0, -1);
    }

    public int loadSound(int id, int priority) {
        return m_soundManager.load(id, priority, -1);
    }

    public int loadSound(int id, int priority, int index) {
        return m_soundManager.load(id, priority, index);
    }

    public void freeSound(int index) {
        m_soundManager.free(index);
    }

    public int playSound(int index) {
        return m_soundManager.play(index, false, 0.5f, 0.5f, 1.0f);
    }

    public int playSound(int index, boolean looping) {
        return m_soundManager.play(index, looping, 0.5f, 0.5f, 1.0f);
    }

    public int playSound(int index, boolean looping, float vol) {
        return m_soundManager.play(index, looping, vol, vol, 1.0f);
    }

    public int playSound(int index, boolean looping, float lvol, float rvol) {
        return m_soundManager.play(index, looping, lvol, rvol, 1.0f);
    }

    public int playSound(int index, boolean looping, float lvol, float rvol, float rate) {
        return m_soundManager.play(index, looping, lvol, rvol, rate);
    }

    public void stopSound(int id) {
        m_soundManager.stop(id);
    }

    public void pauseSound(int id) {
        m_soundManager.pause(id);
    }

    public void resumeSound(int id) {
        m_soundManager.resume(id);
    }

    public void setSoundRate(int id, float rate) {
        m_soundManager.setRate(id, rate);
    }

    public void setSoundVolume(int id, float vol) {
        m_soundManager.setVolume(id, vol, vol);
    }

    public void setSoundVolume(int id, float lvol, float rvol) {
        m_soundManager.setVolume(id, lvol, rvol);
    }

    public int loadMusic(int id) {
        return m_musicManager.load(id, -1);
    }

    public int loadMusic(int id, int index) {
        return m_musicManager.load(id, index);
    }

    public void freeMusic(int index) {
        m_musicManager.free(index);
    }

    public void playMusic(int index) {
        m_musicManager.play(index, false, 0.5f, 0.5f);
    }

    public void playMusic(int index, boolean looping) {
        m_musicManager.play(index, looping, 0.5f, 0.5f);
    }

    public void playMusic(int index, boolean looping, float vol) {
        m_musicManager.play(index, looping, vol, vol);
    }

    public void playMusic(int index, boolean looping, float lvol, float rvol) {
        m_musicManager.play(index, looping, lvol, rvol);
    }

    public void stopMusic(int index) {
        m_musicManager.stop(index);
    }

    public void pauseMusic(int index) {
        m_musicManager.pause(index);
    }

    public void resumeMusic(int index) {
        m_musicManager.resume(index);
    }

    public void setMusicVolume(int index, float vol) {
        m_musicManager.setVolume(index, vol, vol);
    }

    public void setMusicVolume(int index, float lvol, float rvol) {
        m_musicManager.setVolume(index, lvol, rvol);
    }

    public void mute() {
        m_soundManager.stopAll();
        m_musicManager.stopAll();
    }

    public int getKey(int key) {
        return 0;
    }

    public float getOrientation(int index) {
        return m_activity.getOrientation(index);
    }

    public float getAccel(int index) {
        return m_activity.getAccel(index);
    }

    public void saveState(String filename) {
    }

    public void loadState(String filename) {
    }

    public String getDir() {
        return Environment.getExternalStorageDirectory().getPath() + "/vermont";
    }

    public int getPlatform() {
        return PLATFORM_ANDROID;
    }

    public void setTextCacheSize(int textureWidth, int textureHeight, int numCache) {
        m_vtRenderer.setTextCacheSize(textureWidth, textureHeight, numCache);
    }

    public void setTextCacheParams(int index, int x, int y, int fontSizeMin, int fontSizeMax, int renderFontSizeMin, int renderFontSizeMax) {
        m_vtRenderer.setTextCacheParams(index, x, y, fontSizeMin, fontSizeMax, renderFontSizeMin, renderFontSizeMax);
    }

    public Activity getActivity() {
        return m_activity;
    }

    Timer getTimer() {
        return m_timer;
    }

    public String[] getErrorMessages() {
        return m_errorMessages;
    }

    public void enableSound(boolean enable) {
        m_soundManager.enable(enable);
        m_musicManager.enable(enable);
    }

    void postTouchEvent(int action, int count, int[] coord, int[] id) {
        // onUpdate中に別スレッドで実行されると困るので、ここではリストに追加するだけ
        // まとめてdoTouchEventで処理する
        TouchEvent t = new TouchEvent(action, count, coord, id);
        synchronized (m_touchEventList) {
            m_touchEventList.add(t);
        }
    }

    void doTouchEvent() {
        synchronized (m_touchEventList) {
            while (!m_touchEventList.isEmpty()) {
                TouchEvent t = m_touchEventList.removeFirst();
                int action = t.getAction();
                int count = t.getCount();
                int[] coord = t.getCoord();
                int[] id = t.getId();
                onTouchEvent(action, count, coord, id);
            }
        }
    }

    void postBackPressed() {
        // onUpdate中に別スレッドで実行されると困るので、ここではフラグを立てるだけ
        // 後でdoBackPressedで処理する
        m_backPressed = true;
    }

    void doBackPressed() {
        if (m_backPressed) {
            if (onBackPressed())
                m_activity.finish();

            m_backPressed = false;
        }
    }

    void onPause() {
        m_soundManager.onPause();
        m_musicManager.onPause();
    }

    void onResume() {
        m_soundManager.onResume();
        m_musicManager.onResume();
    }

    public void onStart() {
    }

    public void onLoad() {
    }

    protected void onTouchEvent(int action, int count, int[] coord, int[] id) {
    }

    protected boolean onBackPressed() {
        return true;
    }

    public void onUpdate() {
    }

    public void onDraw() {
    }
}
