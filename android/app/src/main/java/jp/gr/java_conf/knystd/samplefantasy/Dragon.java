package jp.gr.java_conf.knystd.samplefantasy;
import android.app.Activity;

import java.util.ArrayList;
import java.util.Random;

import jp.gr.java_conf.knystd.vermont.*;
import jp.gr.java_conf.knystd.vermont.fukujin.*;
import jp.gr.java_conf.knystd.vermont.util.*;


/*------------------------------------------------------------------------------ */
/* Dragon */
/* ドラゴン */
/*------------------------------------------------------------------------------ */
public class Dragon extends Enemy {

    public Dragon(double x, double y, int number) {
        super(x, y, 240, 300, 10, number);
    }

    @Override
    public void draw(VermontExecutor ex) {
        int x = this.getDispX();
        int y = this.getDispY();
        this.drawEnemy(ex, x, y, 1, 681, 240, 300);
        this.drawNumber(ex, x + 120, y + 210, this.getNumber());
        int hp = this.getHp();
        this.drawHp(ex, x + 120, y + 290, hp, 5);
        this.drawHp(ex, x + 120, y + 310, hp - 5, 5);
    }

}
