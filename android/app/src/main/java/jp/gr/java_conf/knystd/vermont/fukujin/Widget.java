package jp.gr.java_conf.knystd.vermont.fukujin;

import java.util.ArrayList;

import jp.gr.java_conf.knystd.vermont.VermontExecutor;

public class Widget {

    public Widget(Widget parent, int x, int y, int width, int height) {
        this.m_parent = parent;
        this.m_x = x;
        this.m_y = y;
        this.m_width = width;
        this.m_height = height;
        this.m_visible = true;
        this.m_children = null;
        this.m_longEar = false;
    }

    public Widget getParent() {
        return this.m_parent;
    }

    public int getX() {
        return this.m_x;
    }

    public void setX(int x) {
        this.m_x = x;
    }

    public int getY() {
        return this.m_y;
    }

    public void setY(int y) {
        this.m_y = y;
    }

    public int getWidth() {
        return this.m_width;
    }

    public void setWidth(int width) {
        this.m_width = width;
    }

    public int getHeight() {
        return this.m_height;
    }

    public void setHeight(int height) {
        this.m_height = height;
    }

    public boolean isVisible() {
        return this.m_visible;
    }

    public void setVisible(boolean visible) {
        this.m_visible = visible;
    }

    public ArrayList<Widget> getChildren() {
        return this.m_children;
    }

    public void addChild(Widget widget) {
        if (this.m_children == null) {
            this.m_children = new ArrayList<>();
        }
        this.m_children.add(widget);
    }

    public void removeChild(Widget widget) {
        if (this.m_children == null) {
            return;
        }

        for (int i = 0; i <= this.m_children.size() - 1; i++) {
            Widget w = this.m_children.get(i);
            if (w == widget) {
                this.m_children.remove(i);
            }
        }
    }

    public boolean isLongEar() {
        return this.m_longEar;
    }

    public void setLongEar(boolean longEar) {
        this.m_longEar = longEar;
    }

    public int getDispX() {
        if (this.m_parent == null) {
            return this.m_x;
        } else {
            return this.m_parent.getDispX() + this.m_x;
        }
    }

    public int getDispY() {
        if (this.m_parent == null) {
            return this.m_y;
        } else {
            return this.m_parent.getDispY() + this.m_y;
        }
    }

    public Widget getHitWidget(int action, int count, int[] coord, int[] id) {
        if (this.m_children != null) {
            for (int i = this.m_children.size() - 1; i >= 0; i += -1) {
                Widget w = this.m_children.get(i);
                w = w.getHitWidget(action, count, coord, id);
                if (w != null) {
                    return w;
                }
            }
        }

        if (this.m_visible && this.isHit(action, count, coord, id)) {
            return this;
        } else {
            return null;
        }
    }

    public void dispatchUpdate() {
        this.onUpdate();

        if (this.m_children != null) {
            for (int i = 0; i <= this.m_children.size() - 1; i++) {
                Widget w = this.m_children.get(i);
                w.dispatchUpdate();
            }
        }
    }

    public void dispatchDraw(VermontExecutor ex) {
        this.onDraw(ex);

        if (this.m_children != null) {
            for (int i = 0; i <= this.m_children.size() - 1; i++) {
                Widget w = this.m_children.get(i);
                if (w.isVisible()) {
                    w.dispatchDraw(ex);
                }
            }
        }
    }

    public void dispatchTouchEvent(int action, int count, int[] coord, int[] id) {
        if (this.isLongEar()) {
            this.onTouchEvent(action, count, coord, id);
        }

        if (this.m_children != null) {
            for (int i = 0; i <= this.m_children.size() - 1; i++) {
                Widget w = this.m_children.get(i);
                w.dispatchTouchEvent(action, count, coord, id);
            }
        }
    }

    public boolean isHit(int action, int count, int[] coord, int[] id) {
        int x1 = coord[0];
        int y1 = coord[1];

        int x2 = this.getDispX();
        int y2 = this.getDispY();
        int w = this.getWidth();
        int h = this.getHeight();

        if (x1 >= x2 && x1 < x2 + w && y1 >= y2 && y1 < y2 + h) {
            return true;
        } else {
            return false;
        }
    }

    public void onTouchEvent(int action, int count, int[] coord, int[] id) {
    }

    public void onUpdate() {
    }

    public void onDraw(VermontExecutor ex) {
    }

    protected Widget m_parent;
    protected int m_x;
    protected int m_y;
    protected int m_width;
    protected int m_height;
    protected boolean m_visible;
    protected ArrayList<Widget> m_children;
    protected boolean m_longEar;

}
