package jp.gr.java_conf.knystd.vermont.util;

import android.content.Context;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

public class TextWriter {
    public static final int MODE_PRIVATE = 0;
    public static final int MODE_EXTERNAL = 1;

    public TextWriter() {
        this.m_bw = null;
    }

    public boolean open(int mode, Context context, String filename) {
        try {
            switch (mode) {
                case MODE_PRIVATE:
                    FileOutputStream fos = context.openFileOutput(filename, Context.MODE_PRIVATE);
                    m_bw = new BufferedWriter(new OutputStreamWriter(fos));
                    break;
                case MODE_EXTERNAL:
                    File file = new File(context.getExternalFilesDir(null), filename);
                    m_bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file)));
                    break;
            }
        } catch (IOException e) {
            m_bw = null;
            return false;
        }

        return true;
    }

    public void close() {
        if (this.m_bw != null) {
            try {
                this.m_bw.close();
            } catch (IOException e) {
            }
            this.m_bw = null;
        }
    }

    public void writeLine(String text) {
        try {
            m_bw.write(text + "\n");
        } catch (IOException e) {
        }
    }

    protected BufferedWriter m_bw;

}
