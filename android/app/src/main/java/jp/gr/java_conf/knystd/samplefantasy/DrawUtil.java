package jp.gr.java_conf.knystd.samplefantasy;
import android.app.Activity;

import java.util.ArrayList;
import java.util.Random;

import jp.gr.java_conf.knystd.vermont.*;
import jp.gr.java_conf.knystd.vermont.fukujin.*;
import jp.gr.java_conf.knystd.vermont.util.*;


/*------------------------------------------------------------------------------ */
/* DrawUtil */
/* 描画用ユーティリティ */
/*------------------------------------------------------------------------------ */
public class DrawUtil {

    public static void drawTime(VermontExecutor ex, int x, int y, int time) {
        int dx = x + 40;
        int second = (int)(time / 60);
        for (int i = 0; i <= 2; i++) {
            int number1 = second % 10;
            second = (int)(second / 10);
            int sx = 385 + number1 * 22;
            ex.blt(dx, y, SampleFantasyApp.IMG_MAIN, sx, 1, 20, 32);
            dx = dx - 20;
        }

        dx = x + 60;
        ex.blt(dx, y, SampleFantasyApp.IMG_MAIN, 363, 1, 20, 32);

        dx = x + 100;
        int decimal = (int)((time % 60) * 100 / 60);
        for (int i = 0; i <= 1; i++) {
            int number1 = decimal % 10;
            decimal = (int)(decimal / 10);
            int sx = 385 + number1 * 22;
            ex.blt(dx, y, SampleFantasyApp.IMG_MAIN, sx, 1, 20, 32);
            dx = dx - 20;
        }
    }

}
