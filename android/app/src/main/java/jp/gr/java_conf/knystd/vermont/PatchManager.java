package jp.gr.java_conf.knystd.vermont;

class PatchManager {
    class Patch {
        private int m_cols;
        private int m_rows;
        private short m_data[];

        Patch(int cols, int rows) {
            m_cols = cols;
            m_rows = rows;
            int len = cols * rows;
            m_data = new short[len];
            for (int i = 0; i < len; i++)
                m_data[i] = -1;
        }

        int getCols() {
            return m_cols;
        }

        int getRows() {
            return m_rows;
        }

        short[] getData() {
            return m_data;
        }
    }

    private int m_patchMax = 0;
    private Patch[] m_patches;

    PatchManager(int patchMax) {
        m_patchMax = patchMax;
        m_patches = new Patch[patchMax];
        init();
    }

    void init() {
        for (int i = 0; i < m_patchMax; i++)
            m_patches[i] = null;
    }

    int createPatch(int cols, int rows, int index) {
        if (index < -1 || index >= m_patchMax)
            return -1;

        if (index < 0) {
            for (int i = 0; i < m_patchMax; i++) {
                if (m_patches[i] == null) {
                    index = i;
                    break;
                }
            }
        }

        if (index < 0)
            return -1;

        freePatch(index);

        m_patches[index] = new Patch(cols, rows);

        return index;
    }

    void freePatch(int index) {
        if (index < 0 || index >= m_patchMax)
            return;

        m_patches[index] = null;
    }

    void clearPatch(int index, short piece) {
        if (index < 0 || index >= m_patchMax)
            return;

        Patch patch = m_patches[index];

        if (patch != null) {
            short[] data = patch.getData();
            int len = patch.getCols() * patch.getRows();
            for (int i = 0; i < len; i++)
                data[i] = piece;
        }
    }

    void setPiece(int index, int col, int row, short piece) {
        if (index < 0 || index >= m_patchMax)
            return;

        Patch patch = m_patches[index];

        if (patch != null) {
            int cols = patch.getCols();
            int rows = patch.getRows();
            if (col >= 0 && col < cols && row >= 0 && row < rows) {
                short[] data = patch.getData();
                data[row * cols + col] = piece;
            }
        }
    }

    short getPiece(int index, int col, int row) {
        if (index < 0 || index >= m_patchMax)
            return -1;

        Patch patch = m_patches[index];

        if (patch != null) {
            int cols = patch.getCols();
            int rows = patch.getRows();
            if (col >= 0 && col < cols && row >= 0 && row < rows) {
                short[] data = patch.getData();
                return data[row * cols + col];
            }
        }

        return -1;
    }

    Patch getPatch(int index) {
        if (index < 0 || index >= m_patchMax)
            return null;

        return m_patches[index];
    }
}
