package jp.gr.java_conf.knystd.vermont.util;

public class GameObject {

    public GameObject(double x, double y, double width, double height) {
        this.m_x = x;
        this.m_y = y;
        this.m_width = width;
        this.m_height = height;
    }

    /* 論理X座標 */
    public double getX() {
        return this.m_x;
    }

    public void setX(double x) {
        this.m_x = x;
    }

    /* 論理Y座標 */
    public double getY() {
        return this.m_y;
    }

    public void setY(double y) {
        this.m_y = y;
    }

    /* 幅 */
    public double getWidth() {
        return this.m_width;
    }

    public void setWidth(double width) {
        this.m_width = width;
    }

    /* 高さ */
    public double getHeight() {
        return this.m_height;
    }

    public void setHeight(double height) {
        this.m_height = height;
    }

    /* 表示X座標 */
    public int getDispX() {
        return (int)(this.m_x);
    }

    public void setDispX(int x) {
        this.m_x = x;
    }

    /* 表示Y座標 */
    public int getDispY() {
        return (int)(this.m_y);
    }

    public void setDispY(int y) {
        this.m_y = y;
    }

    /* 表示幅 */
    public int getDispWidth() {
        return (int)(this.m_width);
    }

    public void setDispWidth(int width) {
        this.m_width = width;
    }

    /* 表示高さ */
    public int getDispHeight() {
        return (int)(this.m_height);
    }

    public void setDispHeight(int height) {
        this.m_height = height;
    }

    private double m_x;
    private double m_y;
    private double m_width;
    private double m_height;
}
