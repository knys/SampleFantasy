package jp.gr.java_conf.knystd.samplefantasy;
import android.app.Activity;

import java.util.ArrayList;
import java.util.Random;

import jp.gr.java_conf.knystd.vermont.*;
import jp.gr.java_conf.knystd.vermont.fukujin.*;
import jp.gr.java_conf.knystd.vermont.util.*;


/*------------------------------------------------------------------------------ */
/* Orc */
/* オーク */
/*------------------------------------------------------------------------------ */
public class Orc extends Enemy {

    public Orc(double x, double y, int number) {
        super(x, y, 130, 130, 3, number);
    }

    @Override
    public void draw(VermontExecutor ex) {
        int x = this.getDispX();
        int y = this.getDispY();
        this.drawEnemy(ex, x, y, 1, 185, 130, 130);
        this.drawNumber(ex, x + 65, y + 90, this.getNumber());
        this.drawHp(ex, x + 65, y + 140, this.getHp(), this.getMaxHp());
    }

}
