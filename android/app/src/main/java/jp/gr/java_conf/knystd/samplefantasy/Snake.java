package jp.gr.java_conf.knystd.samplefantasy;
import android.app.Activity;

import java.util.ArrayList;
import java.util.Random;

import jp.gr.java_conf.knystd.vermont.*;
import jp.gr.java_conf.knystd.vermont.fukujin.*;
import jp.gr.java_conf.knystd.vermont.util.*;


/*------------------------------------------------------------------------------ */
/* Snake */
/* ヘビ */
/*------------------------------------------------------------------------------ */
public class Snake extends Enemy {

    public Snake(double x, double y, int number) {
        super(x, y, 100, 100, 2, number);
    }

    @Override
    public void draw(VermontExecutor ex) {
        int x = this.getDispX();
        int y = this.getDispY();
        this.drawEnemy(ex, x, y, 1, 83, 100, 100);
        this.drawNumber(ex, x + 50, y + 65, this.getNumber());
        this.drawHp(ex, x + 50, y + 110, this.getHp(), this.getMaxHp());
    }

}
