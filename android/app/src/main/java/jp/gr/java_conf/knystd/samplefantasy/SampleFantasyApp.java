package jp.gr.java_conf.knystd.samplefantasy;
import android.app.Activity;

import java.util.ArrayList;
import java.util.Random;

import jp.gr.java_conf.knystd.vermont.*;
import jp.gr.java_conf.knystd.vermont.fukujin.*;
import jp.gr.java_conf.knystd.vermont.util.*;


/*------------------------------------------------------------------------------ */
/* SampleFantasyApp */
/* アプリの骨格となるクラス */
/*------------------------------------------------------------------------------ */
public class SampleFantasyApp extends Application {

    /* 画面の論理解像度 */
    public static final int WIN_WIDTH = 480;
    public static final int WIN_HEIGHT = 700;

    /* 画面の種類 */
    public static final int SCENE_TITLE = 0; /* タイトル画面 */
    public static final int SCENE_GAME = 1; /* ゲーム画面 */

    /* 画像 */
    public static final int IMG_MAIN = 1;

    /* カラーマップ */
    public static final int CM_DAMAGE = 1;

    public SampleFantasyApp(VermontExecutor ex) {
        super(ex);
        this.m_level = 1;
        this.m_bestTime = new int[3];

        ex.setResolution(SampleFantasyApp.WIN_WIDTH, SampleFantasyApp.WIN_HEIGHT); /* 論理解像度設定 */
        ex.setFps(60); /* 秒間60コマで動く */

        ex.loadImage(R.drawable.samplefantasy, SampleFantasyApp.IMG_MAIN);
        this.loadData();

        ex.createColormap(3, SampleFantasyApp.CM_DAMAGE);
        ex.setColormapValue(SampleFantasyApp.CM_DAMAGE, 0, 0, ex.rgb(64, 0, 0));
        ex.setColormapValue(SampleFantasyApp.CM_DAMAGE, 1, 128, ex.rgb(255, 160, 160));
        ex.setColormapValue(SampleFantasyApp.CM_DAMAGE, 2, 255, ex.rgb(255, 255, 255));

        this.setScene(SampleFantasyApp.SCENE_TITLE); /* タイトル画面を表示 */
    }

    public int getLevel() {
        return this.m_level;
    }

    public void setLevel(int level) {
        this.m_level = level;
    }

    public int getBestTime(int level) {
        return this.m_bestTime[level - 1];
    }

    public void setBestTime(int level, int bestTime) {
        this.m_bestTime[level - 1] = bestTime;
    }

    public void setScene(int scene) {
        /* 指定された画面に切り替える */
        if (scene == SampleFantasyApp.SCENE_TITLE) {
            this.setForm(new TitleForm(0, 0, SampleFantasyApp.WIN_WIDTH, SampleFantasyApp.WIN_HEIGHT, this));
        } else if (scene == SampleFantasyApp.SCENE_GAME) {
            this.setForm(new GameForm(0, 0, SampleFantasyApp.WIN_WIDTH, SampleFantasyApp.WIN_HEIGHT, this));
        }
    }

    public void loadData() {
        /* 初期化 */
        for (int i = 0; i <= this.m_bestTime.length - 1; i++) {
            this.m_bestTime[i] = 999 * 60;
        }

        TextReader tr = new TextReader();
        if (tr.open(TextReader.MODE_PRIVATE, this.getExecutor().getActivity(), "besttime.dat")) {
            int index = 0;
            String s = tr.readLine();
            while (s != null && index < this.m_bestTime.length) {
                this.m_bestTime[index] = Integer.parseInt(s);
                index = index + 1;
                s = tr.readLine();
            }
            tr.close();
        }
    }

    public void saveData() {
        TextWriter tw = new TextWriter();
        if (tw.open(TextReader.MODE_PRIVATE, this.getExecutor().getActivity(), "besttime.dat")) {
            for (int i = 0; i <= this.m_bestTime.length - 1; i++) {
                tw.writeLine(String.valueOf(this.m_bestTime[i]));
            }
            tw.close();
        }
    }

    protected int m_level;
    protected int[] m_bestTime;
}
