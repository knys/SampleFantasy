package jp.gr.java_conf.knystd.samplefantasy;
import android.app.Activity;

import java.util.ArrayList;
import java.util.Random;

import jp.gr.java_conf.knystd.vermont.*;
import jp.gr.java_conf.knystd.vermont.fukujin.*;
import jp.gr.java_conf.knystd.vermont.util.*;


/*------------------------------------------------------------------------------ */
/* GameForm */
/* ゲーム画面 */
/*------------------------------------------------------------------------------ */
public class GameForm extends Form {

    public GameForm(int x, int y, int width, int height, SampleFantasyApp app) {
        super(x, y, width, height);

        this.m_app = app;
        this.m_gameCore = new GameCore(app.getExecutor());
    }

    @Override
    public void onInit() {
        this.m_gameCore.init(this.m_app.getLevel());
    }

    @Override
    public void onClose() {
        this.m_gameCore.close();
    }

    @Override
    public void onUpdate() {
        super.onUpdate();

        /* 1フレーム分の更新処理 */
        this.m_gameCore.update();

        if (this.m_gameCore.getMode() == GameCore.MODE_END) {
            if (this.m_gameCore.isCleared()) {
                int time = this.m_gameCore.getTime();
                int level = this.m_app.getLevel();
                if (time < this.m_app.getBestTime(level)) {
                    this.m_app.setBestTime(level, time);
                    this.m_app.saveData();
                }
            }
            this.m_app.setScene(SampleFantasyApp.SCENE_TITLE);
        }
    }

    @Override
    public void onTouchEvent(int action, int count, int[] coord, int[] id) {
        int x = coord[0];
        int y = coord[1];
        if (action == VermontExecutor.ACTION_DOWN) {
            this.m_gameCore.attack(x, y);
        }
    }

    @Override
    public void onDraw(VermontExecutor ex) {
        /* ゲーム画面描画 */
        this.m_gameCore.draw();
    }

    @Override
    public boolean onBackPressed() {
        /* タイトル画面に戻る */
        this.m_app.setScene(SampleFantasyApp.SCENE_TITLE);
        /* アプリは終了しないのでfalseを返す */
        return false;
    }

    protected SampleFantasyApp m_app;
    protected GameCore m_gameCore;
}
