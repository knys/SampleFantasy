package jp.gr.java_conf.knystd.vermont;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.opengl.GLES20;
import android.opengl.GLUtils;

import java.util.ArrayList;
import java.util.List;

class TextTexture {
    private List<TextCache> m_cache;
    private int m_textureId;
    private int m_textureWidth;
    private int m_textureHeight;
    private boolean m_hasTexture;

    class TextCache {
        private int m_renderFontSize;
        private String m_text;
        private int m_x;
        private int m_y;
        private int m_width;
        private int m_height;
        private int m_fontSizeMin;
        private int m_fontSizeMax;
        private int m_renderFontSizeMin;
        private int m_renderFontSizeMax;

        TextCache() {
            m_renderFontSize = 0;
            m_text = "";
            m_x = 0;
            m_y = 0;
            m_width = 0;
            m_height = 0;
            m_fontSizeMin = 0;
            m_fontSizeMax = 0;
            m_renderFontSizeMin = 0;
            m_renderFontSizeMax = 0;
        }

        // TextTexture.get()で指定したサイズと異なる場合があるので注意
        int getRenderFontSize() {
            return m_renderFontSize;
        }

        String getText() {
            return m_text;
        }

        int getX() {
            return m_x;
        }

        int getY() {
            return m_y;
        }

        int getWidth() {
            return m_width;
        }

        int getHeight() {
            return m_height;
        }

        int getFontSizeMin() {
            return m_fontSizeMin;
        }

        int getFontSizeMax() {
            return m_fontSizeMax;
        }

        int getRenderFontSizeMin() {
            return m_renderFontSizeMin;
        }

        int getRenderFontSizeMax() {
            return m_renderFontSizeMax;
        }
    }

    TextTexture() {
        m_cache = new ArrayList<>();
        m_textureId = 0;
        m_hasTexture = false;
    }

    void init() {
        if (m_textureId == 0) {
            int[] textures = new int[1];
            GLES20.glGenTextures(1, textures, 0);
            m_textureId = textures[0];
        }
    }

    int getTextureWidth() {
        return m_textureWidth;
    }

    int getTextureHeight() {
        return m_textureHeight;
    }

    // getした後で使用すること
    int getTextureId() {
        return m_textureId;
    }

    void setTextCacheSize(int textureWidth, int textureHeight, int numCache) {
        m_textureWidth = textureWidth;
        m_textureHeight = textureHeight;
        m_cache.clear();
        for (int i = 0; i < numCache; i++) {
            m_cache.add(new TextCache());
        }
    }

    void setTextCacheParams(int index, int x, int y, int fontSizeMin, int fontSizeMax, int renderFontSizeMin, int renderFontSizeMax) {
        if (index < 0 || index >= m_cache.size())
            return;

        TextCache tc = m_cache.get(index);
        tc.m_x = x;
        tc.m_y = y;
        tc.m_fontSizeMin = fontSizeMin;
        tc.m_fontSizeMax = fontSizeMax;
        tc.m_renderFontSizeMin = renderFontSizeMin;
        tc.m_renderFontSizeMax = renderFontSizeMax;
    }

    TextCache get(String text, int fontSize) {
        TextCache tc = null;
        int renderFontSize = 0;
        boolean hit = false;

        // キャッシュを探す
        for (int i = m_cache.size() - 1; i >= 0; i--) {
            TextCache tmp = m_cache.get(i);
            if (fontSize >= tmp.m_fontSizeMin && fontSize <= tmp.m_fontSizeMax) {
                // 指定フォントサイズが範囲内なら候補
                tc = tmp;
                renderFontSize = fontSize;
                if (renderFontSize < tc.m_renderFontSizeMin)
                    renderFontSize = tc.m_renderFontSizeMin;
                if (renderFontSize > tc.m_renderFontSizeMax)
                    renderFontSize = tc.m_renderFontSizeMax;
                if (renderFontSize == tc.m_renderFontSize && text.equals(tc.m_text)) {
                    // キャッシュにヒットしたら抜ける
                    hit = true;
                    break;
                }
            }
        }

        // 使用できるTextCacheが見つからない場合(設定がおかしい)
        if (tc == null)
            return null;

        // キャッシュの末尾に持ってくる
        m_cache.remove(tc);
        m_cache.add(tc);

        // キャッシュになかった場合は新規作成
        if (!hit) {
            createText(tc, text, renderFontSize);
        }

        return tc;
    }

    private void createText(TextCache tc, String text, int fontSize) {
        initTexture();

        tc.m_text = text;
        tc.m_renderFontSize = fontSize;

        // 文字列サイズ取得
        Paint paint = new Paint();
        paint.setTextSize(fontSize);
        paint.setColor(Color.WHITE);
        paint.setAntiAlias(true);
        int width = (int) Math.ceil(paint.measureText(text));
        if (width > m_textureWidth - tc.m_x)
            width = m_textureWidth - tc.m_x;
        float ascent = paint.ascent();
        float descent = paint.descent();
        int height = (int) Math.ceil(descent - ascent);
        if (height > m_textureHeight - tc.m_y)
            height = m_textureHeight - tc.m_y;
        tc.m_width = width;
        tc.m_height = height;

        // テクスチャ作成
        text2texture(width, height, text, ascent, paint, tc);
    }

    private void initTexture() {
        if (m_hasTexture)
            return;

        createTexture();

        m_hasTexture = true;
    }

    private void createTexture() {
        GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, m_textureId);
        // GL_TEXTURE_MIN_FILTERやGL_TEXTURE_MAG_FILTERを設定しておかないと実機で表示されないことがあるらしい
        GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_NEAREST);
        GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_NEAREST);
        Bitmap bitmap = Bitmap.createBitmap(m_textureWidth, m_textureHeight, Bitmap.Config.ARGB_8888);
        bitmap.eraseColor(0);
        GLUtils.texImage2D(GLES20.GL_TEXTURE_2D, 0, bitmap, 0);
        bitmap.recycle();
    }

    private void text2texture(int width, int height, String text, float ascent, Paint paint, TextCache tc) {
        // ビットマップに文字列描画
        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        bitmap.eraseColor(0);
        Canvas canvas = new Canvas(bitmap);
        canvas.drawText(text, 0, -ascent, paint);

        GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, m_textureId);
        GLUtils.texSubImage2D(GLES20.GL_TEXTURE_2D, 0, tc.m_x, tc.m_y, bitmap);
        bitmap.recycle();
    }

    void reload() {
        if (!m_hasTexture)
            return;

        createTexture();

        Paint paint = new Paint();
        paint.setColor(Color.WHITE);
        paint.setAntiAlias(true);

        for (TextCache tc : m_cache) {
            if (!tc.m_text.equals("")) {
                paint.setTextSize(tc.m_renderFontSize);
                float ascent = paint.ascent();

                text2texture(tc.m_width, tc.m_height, tc.m_text, ascent, paint, tc);
            }
        }
    }
}
