package jp.gr.java_conf.knystd.vermont.fukujin;

import jp.gr.java_conf.knystd.vermont.VermontExecutor;

public class Slider extends Widget {

    public Slider(Widget parent, int x, int y, int width, int height, int value, int min, int max, int step) {
        super(parent, x, y, width, height);
        this.m_value = value;
        this.m_min = min;
        this.m_max = max;
        this.m_step = step;

        this.setValue(value);
    }

    public int getValue() {
        return this.m_value;
    }

    public void setValue(int value) {
        if (value < this.m_min) {
            value = this.m_min;
        }

        int geta = (int) (this.m_step / 2);
        int tmp = value - this.m_min + geta;
        this.m_value = this.m_min + (int) (tmp / this.m_step) * this.m_step;

        if (this.m_value > this.m_max) {
            this.m_value = this.m_max;
        }
    }

    public int getMin() {
        return this.m_min;
    }

    public int getMax() {
        return this.m_max;
    }

    @Override
    public void onTouchEvent(int action, int count, int[] coord, int[] id) {
        if (action == VermontExecutor.ACTION_DOWN || action == VermontExecutor.ACTION_MOVE || action == VermontExecutor.ACTION_UP) {
            int x1 = coord[0];
            int x2 = this.getDispX();
            int w = this.getWidth();
            int value = this.m_min + (int) ((this.m_max - this.m_min) * (x1 - x2) / w);
            this.setValue(value);
            this.onChange();
        }
    }

    @Override
    public void onDraw(VermontExecutor ex) {
        int x = this.getDispX();
        int y = this.getDispY();
        int w = this.getWidth();
        int h = this.getHeight();

        int c = ex.setColor(ex.rgb(255, 255, 255));
        ex.drawRect(x, y, w, h);
        int x1 = x + (int) (w * (this.m_value - this.m_min) / (this.m_max - this.m_min));
        ex.drawLine(x1, y, x1, y + h - 1);
        ex.setColor(c);
    }

    public void onChange() {
    }

    protected int m_value;
    protected int m_min;
    protected int m_max;
    protected int m_step;

}
