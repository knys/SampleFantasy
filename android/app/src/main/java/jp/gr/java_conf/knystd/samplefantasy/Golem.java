package jp.gr.java_conf.knystd.samplefantasy;
import android.app.Activity;

import java.util.ArrayList;
import java.util.Random;

import jp.gr.java_conf.knystd.vermont.*;
import jp.gr.java_conf.knystd.vermont.fukujin.*;
import jp.gr.java_conf.knystd.vermont.util.*;


/*------------------------------------------------------------------------------ */
/* Golem */
/* ゴーレム */
/*------------------------------------------------------------------------------ */
public class Golem extends Enemy {

    public Golem(double x, double y, int number) {
        super(x, y, 180, 200, 5, number);
    }

    @Override
    public void draw(VermontExecutor ex) {
        int x = this.getDispX();
        int y = this.getDispY();
        this.drawEnemy(ex, x, y, 1, 479, 180, 200);
        this.drawNumber(ex, x + 90, y + 150, this.getNumber());
        this.drawHp(ex, x + 90, y + 210, this.getHp(), this.getMaxHp());
    }

}
