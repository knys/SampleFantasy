package jp.gr.java_conf.knystd.vermont;

import android.content.res.Resources;
import android.opengl.GLES20;
import android.opengl.GLException;
import android.opengl.GLSurfaceView.Renderer;
import android.os.Build;
import android.util.Log;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

class VermontRenderer implements Renderer {
    private VermontExecutor m_executor;
    private int m_viewWidth;
    private int m_viewHeight;
    private int m_marginX;
    private int m_marginY;
    private int m_color;
    private int m_blendMode;
    private int m_operation;
    private int m_alpha;
    private int m_fontSize;
    private int m_textHAlign;
    private int m_textVAlign;
    private int m_maskImage;
    private int m_maskDx;
    private int m_maskDy;
    private int m_maskDw;
    private int m_maskDh;
    private int m_maskSx;
    private int m_maskSy;
    private int m_maskSw;
    private int m_maskSh;

    private int m_textureMax;
    private Texture[] m_texture;
    private TextTexture m_textTexture;

    private FloatBuffer m_floatBuf;
    private float[] m_floatVector;
    private float[] m_vertices;
    private int m_bufferId;

    private boolean m_enableFrameSkip = false;
    private boolean m_showLoad = false;

    private static final int VERTEX_MAX = 120; // 最大の頂点数
    private static final int FLOAT_BYTES = 4; // floatのバイト数
    private static final String API_LEVEL = "$API_LEVEL$"; // シェーダでのAPIレベル指定用

    private class Shader {
        static final int POSITION = 0x0001;
        static final int TEX_POSITION = 0x0002;
        static final int TRANSFORM = 0x0004;
        static final int SAMPLER = 0x0008;
        static final int FRAG_COLOR = 0x0010;
        static final int TEX_SIZE = 0x0020;
        static final int COLORMAP_BORDER = 0x0040;
        static final int COLORMAP_COLOR = 0x0080;
        static final int COLORMAP_OFFSET = 0x0100;
        static final int MASK_TRANSFORM = 0x0200;
        static final int MASK_SAMPLER = 0x0400;
        static final int MASK_RANGE = 0x0800;

        Shader() {
            m_program = -1;
            m_aPosition = -1;
            m_aTexPosition = -1;
            m_uTransform = -1;
            m_uSampler = -1;
            m_uFragColor = -1;
            m_uTexSize = -1;
            m_uColormapBorder = -1;
            m_uColormapColor = -1;
            m_uColormapCount = -1;
            m_uColormapOffset = -1;
            m_uMaskTransform = -1;
            m_uMaskSampler = -1;
            m_uMaskRange = -1;
        }

        int m_program;
        int m_aPosition;
        int m_aTexPosition;
        int m_uTransform;
        int m_uSampler;
        int m_uFragColor;
        int m_uTexSize;
        int m_uColormapBorder;
        int m_uColormapColor;
        int m_uColormapCount;
        int m_uColormapOffset;
        int m_uMaskTransform;
        int m_uMaskSampler;
        int m_uMaskRange;
    }

    private Shader m_shaderS;
    private Shader m_shaderT;
    private Shader m_shaderC;
    private Shader m_shaderSM;
    private Shader m_shaderTM;
    private Shader m_shaderCM;

    VermontRenderer(Resources res, VermontExecutor executor, int textureMax) {
        m_executor = executor;
        m_viewHeight = 0;
        m_textureMax = textureMax;
        m_texture = new Texture[m_textureMax];
        for (int i = 0; i < m_textureMax; i++)
            m_texture[i] = new Texture(res);
        m_textTexture = new TextTexture();
        m_floatBuf = createFloatBuffer(VERTEX_MAX);
        m_vertices = new float[VERTEX_MAX * 2];
        m_floatVector = new float[4];
    }

    void init() {
        for (int i = 0; i < m_textureMax; i++)
            m_texture[i].delete();
    }

    void enableFrameSkip(boolean b) {
        m_enableFrameSkip = b;
    }

    void showLoad(boolean b) {
        m_showLoad = b;
    }

    int loadImage(int id, int index) {
        if (index < -1 || index >= m_textureMax) {
            return -1;
        } else {
            if (index < 0) {
                for (int i = 0; i < m_textureMax; i++) {
                    if (m_texture[i].isUsable()) {
                        index = i;
                        break;
                    }
                }
                if (index < 0) {
                    return -1;
                }
            }
            m_texture[index].load(id);
        }
        return index;
    }

    void freeImage(int index) {
        if (index < 0 || index >= m_textureMax)
            return;

        m_texture[index].delete();
    }

    void getImageSize(int img, int[] size) {
        size[0] = m_texture[img].getWidth();
        size[1] = m_texture[img].getHeight();
    }

    int setColor(int color) {
        int c = m_color;
        m_color = color;
        return c;
    }

    int setBlendMode(int blendMode) {
        int b = m_blendMode;
        m_blendMode = blendMode;
        return b;
    }

    int setTextureOperation(int operation) {
        int o = m_operation;
        m_operation = operation;
        return o;
    }

    int setAlpha(int alpha) {
        int a = m_alpha;
        m_alpha = alpha;
        return a;
    }

    void cls(int color) {
        float r = (color >>> 16 & 0x0ff) / 255f;
        float g = (color >>> 8 & 0x0ff) / 255f;
        float b = (color & 0x0ff) / 255f;
        GLES20.glClearColor(r, g, b, 1.0f);
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT);
    }

    int setFontSize(int size) {
        int f = m_fontSize;
        m_fontSize = size;
        return f;
    }

    int setTextHAlign(int halign) {
        int ha = m_textHAlign;
        m_textHAlign = halign;
        return ha;
    }

    int setTextVAlign(int valign) {
        int va = m_textVAlign;
        m_textVAlign = valign;
        return va;
    }

    void setMask(int dx, int dy, int dw, int dh, int img, int sx, int sy, int sw, int sh) {
        m_maskDx = dx;
        m_maskDy = dy;
        m_maskDw = dw;
        m_maskDh = dh;
        m_maskImage = img;
        m_maskSx = sx;
        m_maskSy = sy;
        m_maskSw = sw;
        m_maskSh = sh;
    }

    void clearMask() {
        m_maskImage = -1;
    }

    void drawText(int x, int y, String text) {
        if (text.equals(""))
            return;

        TextTexture.TextCache tc = m_textTexture.get(text, m_fontSize);

        int sx = tc.getX();
        int sy = tc.getY();
        int sw = tc.getWidth();
        int sh = tc.getHeight();
        int textureFontSize = tc.getRenderFontSize();
        int dw = sw * m_fontSize / textureFontSize;
        switch (m_textHAlign) {
            case VermontExecutor.HALIGN_CENTER:
                x -= dw / 2;
                break;
            case VermontExecutor.HALIGN_RIGHT:
                x -= dw;
                break;
        }
        int dh = sh * m_fontSize / textureFontSize;
        switch (m_textVAlign) {
            case VermontExecutor.VALIGN_MIDDLE:
                y -= dh / 2;
                break;
            case VermontExecutor.VALIGN_BOTTOM:
                y -= dh;
                break;
        }
        setBltFloatBuffer(m_floatBuf, x, y, dw, dh, sx, sy, sw, sh);

        Shader shader;
        if (m_maskImage >= 0) {
            shader = m_shaderTM;
        } else {
            shader = m_shaderT;
        }
        int op = setTextureOperation(VermontExecutor.OP_MODULATE);
        beginDrawTexture(shader, m_textTexture.getTextureId(), m_textTexture.getTextureWidth(), m_textTexture.getTextureHeight());
        GLES20.glBufferData(GLES20.GL_ARRAY_BUFFER, FLOAT_BYTES * 4 * 4, m_floatBuf, GLES20.GL_DYNAMIC_DRAW);
        GLES20.glVertexAttribPointer(shader.m_aPosition, 2, GLES20.GL_FLOAT, false, FLOAT_BYTES * 4, 0);
        GLES20.glVertexAttribPointer(shader.m_aTexPosition, 2, GLES20.GL_FLOAT, false, FLOAT_BYTES * 4, FLOAT_BYTES * 2);
        GLES20.glDrawArrays(GLES20.GL_TRIANGLE_STRIP, 0, 4);
        endDrawTexture(shader);
        setTextureOperation(op);
    }

    void drawLine(int x1, int y1, int x2, int y2) {
        m_vertices[0] = x1;
        m_vertices[1] = y1;
        m_vertices[2] = x2;
        m_vertices[3] = y2;
        setFloatBuffer(m_floatBuf, m_vertices, 4);
        drawSolidShape(GLES20.GL_LINE_STRIP, 2, false);
    }

    void drawRect(int x, int y, int w, int h) {
        // なぜか微調整が必要
        m_vertices[0] = x + w;
        m_vertices[1] = y + h;
        m_vertices[2] = x + 1;
        m_vertices[3] = y + h;
        m_vertices[4] = x + 1;
        m_vertices[5] = y + 1;
        m_vertices[6] = x + w;
        m_vertices[7] = y + 1;
        m_vertices[8] = x + w;
        m_vertices[9] = y + h;
        setFloatBuffer(m_floatBuf, m_vertices, 10);
        drawSolidShape(GLES20.GL_LINE_STRIP, 5, false);
    }

    void fillRect(int x, int y, int w, int h) {
        m_vertices[0] = x;
        m_vertices[1] = y;
        m_vertices[2] = x + w;
        m_vertices[3] = y;
        m_vertices[4] = x;
        m_vertices[5] = y + h;
        m_vertices[6] = x + w;
        m_vertices[7] = y + h;
        setFloatBuffer(m_floatBuf, m_vertices, 8);
        drawSolidShape(GLES20.GL_TRIANGLE_STRIP, 4, true);
    }

    void drawEllipse(int x, int y, int w, int h) {
        drawOrFillEllipse(false, x, y, w, h);
    }

    void fillEllipse(int x, int y, int w, int h) {
        drawOrFillEllipse(true, x, y, w, h);
    }

    private void drawOrFillEllipse(boolean fill, int x, int y, int w, int h) {
        int ndiv; // PI/2内の頂点数
        if (w < h) {
            ndiv = w / 2;
        } else {
            ndiv = h / 2;
        }
        if (ndiv < 1)
            ndiv = 1;
        // とりあえず最大32角形
        if (ndiv > 8)
            ndiv = 8;
        setEllipseFloatBuffer(m_floatBuf, x, y, w, h, ndiv);
        drawSolidShape(fill ? GLES20.GL_TRIANGLE_FAN : GLES20.GL_LINE_LOOP, ndiv * 4, fill);
    }

    void drawPolygon(int[] vertices) {
        drawOrFillPolygon(false, vertices);
    }

    void fillPolygon(int[] vertices) {
        drawOrFillPolygon(true, vertices);
    }

    private void drawOrFillPolygon(boolean fill, int[] vertices) {
        for (int i = 0; i < 6; i++)
            m_vertices[i] = vertices[i];
        setFloatBuffer(m_floatBuf, m_vertices, 6);
        drawSolidShape(fill ? GLES20.GL_TRIANGLE_STRIP : GLES20.GL_LINE_LOOP, 3, fill);
    }

    void texturePolygon(int[] vertices, int img, int[] coords) {
        for (int i = 0; i < 3; i++) {
            m_vertices[i * 4] = vertices[i * 2];
            m_vertices[i * 4 + 1] = vertices[i * 2 + 1];
            m_vertices[i * 4 + 2] = coords[i * 2];
            m_vertices[i * 4 + 3] = coords[i * 2 + 1];
        }
        setFloatBuffer(m_floatBuf, m_vertices, 12);

        Shader shader = getTexturedShader();
        Texture tex = m_texture[img];
        beginDrawTexture(shader, tex.getTextureId(), tex.getWidth(), tex.getHeight());
        GLES20.glBufferData(GLES20.GL_ARRAY_BUFFER, FLOAT_BYTES * 4 * 3, m_floatBuf, GLES20.GL_DYNAMIC_DRAW);
        GLES20.glVertexAttribPointer(shader.m_aPosition, 2, GLES20.GL_FLOAT, false, FLOAT_BYTES * 4, 0);
        GLES20.glVertexAttribPointer(shader.m_aTexPosition, 2, GLES20.GL_FLOAT, false, FLOAT_BYTES * 4, FLOAT_BYTES * 2);
        GLES20.glDrawArrays(GLES20.GL_TRIANGLE_STRIP, 0, 3);
        endDrawTexture(shader);
    }

    void blt(int dx, int dy, int img, int sx, int sy, int sw, int sh) {
        stretchBlt(dx, dy, sw, sh, img, sx, sy, sw, sh);
    }

    void stretchBlt(int dx, int dy, int dw, int dh, int img, int sx, int sy, int sw, int sh) {
        if (dw < 0) {
            sx += sw;
            sw = -sw;
            dw = -dw;
        }
        if (dh < 0) {
            sy += sh;
            sh = -sh;
            dh = -dh;
        }
        setBltFloatBuffer(m_floatBuf, dx, dy, dw, dh, sx, sy, sw, sh);

        Shader shader = getTexturedShader();
        Texture tex = m_texture[img];
        beginDrawTexture(shader, tex.getTextureId(), tex.getWidth(), tex.getHeight());
        GLES20.glBufferData(GLES20.GL_ARRAY_BUFFER, FLOAT_BYTES * 4 * 4, m_floatBuf, GLES20.GL_DYNAMIC_DRAW);
        GLES20.glVertexAttribPointer(shader.m_aPosition, 2, GLES20.GL_FLOAT, false, FLOAT_BYTES * 4, 0);
        GLES20.glVertexAttribPointer(shader.m_aTexPosition, 2, GLES20.GL_FLOAT, false, FLOAT_BYTES * 4, FLOAT_BYTES * 2);
        GLES20.glDrawArrays(GLES20.GL_TRIANGLE_STRIP, 0, 4);
        endDrawTexture(shader);
    }

    void tileBlt(int dx, int dy, int dw, int dh, int img, int sx, int sy, int sw, int sh, int ox, int oy) {
        Shader shader = getTexturedShader();
        Texture tex = m_texture[img];
        beginDrawTexture(shader, tex.getTextureId(), tex.getWidth(), tex.getHeight());

        int vertexIndex = 0;
        int vertexMax = VERTEX_MAX / 12 * 12 * 2; // 四角形1つにつき12頂点必要なので12*2の倍数になるようにする
        int sy1 = sy + oy;
        int sh1 = sh - oy;
        for (int dy1 = dy; dy1 < dy + dh; dy1 += sh1, sy1 = sy, sh1 = sh) {
            if (dy1 + sh1 > dy + dh)
                sh1 -= (dy1 + sh1) - (dy + dh);

            int sx1 = sx + ox;
            int sw1 = sw - ox;
            for (int dx1 = dx; dx1 < dx + dw; dx1 += sw1, sx1 = sx, sw1 = sw) {
                if (dx1 + sw1 > dx + dw)
                    sw1 -= (dx1 + sw1) - (dx + dw);

                m_vertices[vertexIndex++] = dx1;
                m_vertices[vertexIndex++] = dy1;
                m_vertices[vertexIndex++] = sx1;
                m_vertices[vertexIndex++] = sy1;
                m_vertices[vertexIndex++] = dx1 + sw1;
                m_vertices[vertexIndex++] = dy1;
                m_vertices[vertexIndex++] = sx1 + sw1;
                m_vertices[vertexIndex++] = sy1;
                m_vertices[vertexIndex++] = dx1;
                m_vertices[vertexIndex++] = dy1 + sh1;
                m_vertices[vertexIndex++] = sx1;
                m_vertices[vertexIndex++] = sy1 + sh1;
                m_vertices[vertexIndex++] = dx1 + sw1;
                m_vertices[vertexIndex++] = dy1;
                m_vertices[vertexIndex++] = sx1 + sw1;
                m_vertices[vertexIndex++] = sy1;
                m_vertices[vertexIndex++] = dx1;
                m_vertices[vertexIndex++] = dy1 + sh1;
                m_vertices[vertexIndex++] = sx1;
                m_vertices[vertexIndex++] = sy1 + sh1;
                m_vertices[vertexIndex++] = dx1 + sw1;
                m_vertices[vertexIndex++] = dy1 + sh1;
                m_vertices[vertexIndex++] = sx1 + sw1;
                m_vertices[vertexIndex++] = sy1 + sh1;
                if (vertexIndex == vertexMax) {
                    setFloatBuffer(m_floatBuf, m_vertices, vertexIndex);
                    GLES20.glBufferData(GLES20.GL_ARRAY_BUFFER, FLOAT_BYTES * vertexIndex, m_floatBuf, GLES20.GL_DYNAMIC_DRAW);
                    GLES20.glVertexAttribPointer(shader.m_aPosition, 2, GLES20.GL_FLOAT, false, FLOAT_BYTES * 4, 0);
                    GLES20.glVertexAttribPointer(shader.m_aTexPosition, 2, GLES20.GL_FLOAT, false, FLOAT_BYTES * 4, FLOAT_BYTES * 2);
                    GLES20.glDrawArrays(GLES20.GL_TRIANGLES, 0, vertexIndex / 4);
                    vertexIndex = 0;
                }
            }
        }
        if (vertexIndex > 0) {
            setFloatBuffer(m_floatBuf, m_vertices, vertexIndex);
            GLES20.glBufferData(GLES20.GL_ARRAY_BUFFER, FLOAT_BYTES * vertexIndex, m_floatBuf, GLES20.GL_DYNAMIC_DRAW);
            GLES20.glVertexAttribPointer(shader.m_aPosition, 2, GLES20.GL_FLOAT, false, FLOAT_BYTES * 4, 0);
            GLES20.glVertexAttribPointer(shader.m_aTexPosition, 2, GLES20.GL_FLOAT, false, FLOAT_BYTES * 4, FLOAT_BYTES * 2);
            GLES20.glDrawArrays(GLES20.GL_TRIANGLES, 0, vertexIndex / 4);
        }

        endDrawTexture(shader);
    }

    void rotateBlt(int[] vertices, int img, int sx, int sy, int sw, int sh) {
        m_vertices[0] = vertices[0];
        m_vertices[1] = vertices[1];
        m_vertices[2] = sx;
        m_vertices[3] = sy;
        m_vertices[4] = vertices[2];
        m_vertices[5] = vertices[3];
        m_vertices[6] = sx + sw;
        m_vertices[7] = sy;
        m_vertices[8] = vertices[4];
        m_vertices[9] = vertices[5];
        m_vertices[10] = sx;
        m_vertices[11] = sy + sh;
        m_vertices[12] = vertices[6];
        m_vertices[13] = vertices[7];
        m_vertices[14] = sx + sw;
        m_vertices[15] = sy + sh;
        setFloatBuffer(m_floatBuf, m_vertices, 16);

        Shader shader = getTexturedShader();
        Texture tex = m_texture[img];
        beginDrawTexture(shader, tex.getTextureId(), tex.getWidth(), tex.getHeight());
        GLES20.glBufferData(GLES20.GL_ARRAY_BUFFER, FLOAT_BYTES * 4 * 4, m_floatBuf, GLES20.GL_DYNAMIC_DRAW);
        GLES20.glVertexAttribPointer(shader.m_aPosition, 2, GLES20.GL_FLOAT, false, FLOAT_BYTES * 4, 0);
        GLES20.glVertexAttribPointer(shader.m_aTexPosition, 2, GLES20.GL_FLOAT, false, FLOAT_BYTES * 4, FLOAT_BYTES * 2);
        GLES20.glDrawArrays(GLES20.GL_TRIANGLE_STRIP, 0, 4);
        endDrawTexture(shader);
    }

    void patchBlt(int dx, int dy, PatchManager.Patch patch, int img, int sx, int sy, int pw, int ph, int scols, int srows) {
        int dw = pw * patch.getCols();
        int dh = ph * patch.getRows();
        patchStretchBlt(dx, dy, dw, dh, patch, img, sx, sy, pw, ph, scols, srows);
    }

    void patchStretchBlt(int dx, int dy, int dw, int dh, PatchManager.Patch patch, int img, int sx, int sy, int pw, int ph, int scols, int srows) {
        Shader shader = getTexturedShader();
        Texture tex = m_texture[img];
        beginDrawTexture(shader, tex.getTextureId(), tex.getWidth(), tex.getHeight());

        int width = m_executor.getWidth();
        int height = m_executor.getHeight();
        short[] data = patch.getData();
        int patchIndex = 0;
        int colMax = patch.getCols();
        int rowMax = patch.getRows();
        int vertexIndex = 0;
        int vertexMax = VERTEX_MAX / 12 * 12 * 2; // 四角形1つにつき12頂点必要なので12*2の倍数になるようにする
        int dy1 = dy;
        for (int row = 0; row < rowMax; row++) {
            int dy2 = dy + dh * (row + 1) / rowMax;
            int dh1 = dy2 - dy1;
            int dx1 = dx;
            for (int col = 0; col < colMax; col++) {
                int dx2 = dx + dw * (col + 1) / colMax;
                int dw1 = dx2 - dx1;
                short piece = data[patchIndex];
                if (piece >= 0 && dx1 + dw1 > 0 && dx1 < width && dy1 + dh1 > 0 && dy1 < height) {
                    int sx1 = sx + (piece % scols) * (pw + 2);
                    int sy1 = sy + (piece / scols) * (ph + 2);
                    m_vertices[vertexIndex++] = dx1;
                    m_vertices[vertexIndex++] = dy1;
                    m_vertices[vertexIndex++] = sx1;
                    m_vertices[vertexIndex++] = sy1;
                    m_vertices[vertexIndex++] = dx1 + dw1;
                    m_vertices[vertexIndex++] = dy1;
                    m_vertices[vertexIndex++] = sx1 + pw;
                    m_vertices[vertexIndex++] = sy1;
                    m_vertices[vertexIndex++] = dx1;
                    m_vertices[vertexIndex++] = dy1 + dh1;
                    m_vertices[vertexIndex++] = sx1;
                    m_vertices[vertexIndex++] = sy1 + ph;
                    m_vertices[vertexIndex++] = dx1 + dw1;
                    m_vertices[vertexIndex++] = dy1;
                    m_vertices[vertexIndex++] = sx1 + pw;
                    m_vertices[vertexIndex++] = sy1;
                    m_vertices[vertexIndex++] = dx1;
                    m_vertices[vertexIndex++] = dy1 + dh1;
                    m_vertices[vertexIndex++] = sx1;
                    m_vertices[vertexIndex++] = sy1 + ph;
                    m_vertices[vertexIndex++] = dx1 + dw1;
                    m_vertices[vertexIndex++] = dy1 + dh1;
                    m_vertices[vertexIndex++] = sx1 + pw;
                    m_vertices[vertexIndex++] = sy1 + ph;
                    if (vertexIndex == vertexMax) {
                        setFloatBuffer(m_floatBuf, m_vertices, vertexIndex);
                        GLES20.glBufferData(GLES20.GL_ARRAY_BUFFER, FLOAT_BYTES * vertexIndex, m_floatBuf, GLES20.GL_DYNAMIC_DRAW);
                        GLES20.glVertexAttribPointer(shader.m_aPosition, 2, GLES20.GL_FLOAT, false, FLOAT_BYTES * 4, 0);
                        GLES20.glVertexAttribPointer(shader.m_aTexPosition, 2, GLES20.GL_FLOAT, false, FLOAT_BYTES * 4, FLOAT_BYTES * 2);
                        GLES20.glDrawArrays(GLES20.GL_TRIANGLES, 0, vertexIndex / 4);
                        vertexIndex = 0;
                    }
                }
                dx1 = dx2;
                patchIndex++;
            }
            dy1 = dy2;
        }
        if (vertexIndex > 0) {
            setFloatBuffer(m_floatBuf, m_vertices, vertexIndex);
            GLES20.glBufferData(GLES20.GL_ARRAY_BUFFER, FLOAT_BYTES * vertexIndex, m_floatBuf, GLES20.GL_DYNAMIC_DRAW);
            GLES20.glVertexAttribPointer(shader.m_aPosition, 2, GLES20.GL_FLOAT, false, FLOAT_BYTES * 4, 0);
            GLES20.glVertexAttribPointer(shader.m_aTexPosition, 2, GLES20.GL_FLOAT, false, FLOAT_BYTES * 4, FLOAT_BYTES * 2);
            GLES20.glDrawArrays(GLES20.GL_TRIANGLES, 0, vertexIndex / 4);
        }

        endDrawTexture(shader);
    }

    void setTextCacheSize(int textureWidth, int textureHeight, int numCache) {
        m_textTexture.setTextCacheSize(textureWidth, textureHeight, numCache);
    }

    void setTextCacheParams(int index, int x, int y, int fontSizeMin, int fontSizeMax, int renderFontSizeMin, int renderFontSizeMax) {
        m_textTexture.setTextCacheParams(index, x, y, fontSizeMin, fontSizeMax, renderFontSizeMin, renderFontSizeMax);
    }

    void initTextCache() {
        setTextCacheSize(512, 512, 16);
        for (int i = 0; i < 16; i++) {
            setTextCacheParams(i, 0, i * 32, 0, 999, 1, 25);
        }
    }

    private void prepareSolidColorBlend(Shader shader, boolean blend) {
        float a = blend ? (m_color >>> 24) / 255f : 1f;
        float r = (m_color >>> 16 & 0x0ff) / 255f;
        float g = (m_color >>> 8 & 0x0ff) / 255f;
        float b = (m_color & 0x0ff) / 255f;
        if (!blend || m_blendMode == VermontExecutor.BLEND_NONE) {
            GLES20.glBlendFunc(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA);
        } else {
            a = a * m_alpha / 255f;
            switch (m_blendMode) {
                case VermontExecutor.BLEND_NORMAL:
                    GLES20.glBlendFunc(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA);
                    break;
                case VermontExecutor.BLEND_ADD:
                    GLES20.glBlendFunc(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE);
                    break;
            }
        }

        GLES20.glUniform4f(shader.m_uFragColor, r, g, b, a);
    }

    private void prepareTextureBlend(Shader shader) {
        float a;
        float r;
        float g;
        float b;
        if (m_operation == VermontExecutor.OP_REPLACE) {
            r = 1f;
            g = 1f;
            b = 1f;
        } else {
            r = (m_color >>> 16 & 0x0ff) / 255f;
            g = (m_color >>> 8 & 0x0ff) / 255f;
            b = (m_color & 0x0ff) / 255f;
        }

        if (m_blendMode == VermontExecutor.BLEND_NONE) {
            a = 1f;
            GLES20.glBlendFunc(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA);
        } else {
            a = m_alpha / 255f;
            switch (m_blendMode) {
                case VermontExecutor.BLEND_NORMAL:
                    GLES20.glBlendFunc(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA);
                    break;
                case VermontExecutor.BLEND_ADD:
                    GLES20.glBlendFunc(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE);
                    break;
            }
        }

        GLES20.glUniform4f(shader.m_uFragColor, r, g, b, a);
    }

    private void setFloatBuffer(FloatBuffer buf, float[] vertices, int count) {
        buf.position(0);
        buf.put(vertices, 0, count);
        buf.position(0);
    }

    private void setBltFloatBuffer(FloatBuffer buf, int dx, int dy, int dw, int dh, int sx, int sy, int sw, int sh) {
        m_vertices[0] = dx;
        m_vertices[1] = dy;
        m_vertices[2] = sx;
        m_vertices[3] = sy;
        m_vertices[4] = dx + dw;
        m_vertices[5] = dy;
        m_vertices[6] = sx + sw;
        m_vertices[7] = sy;
        m_vertices[8] = dx;
        m_vertices[9] = dy + dh;
        m_vertices[10] = sx;
        m_vertices[11] = sy + sh;
        m_vertices[12] = dx + dw;
        m_vertices[13] = dy + dh;
        m_vertices[14] = sx + sw;
        m_vertices[15] = sy + sh;
        setFloatBuffer(buf, m_vertices, 16);
    }

    private void setEllipseFloatBuffer(FloatBuffer buf, int x, int y, int w, int h, int ndiv) {
        int ox = x + w / 2;
        int oy = y + h / 2;
        int w2 = w / 2;
        int h2 = h / 2;
        for (int i = 0; i < ndiv; i++) {
            double sin = Math.sin(Math.PI / 2 * i / ndiv);
            double cos = Math.cos(Math.PI / 2 * i / ndiv);
            int sinw = (int) (sin * w2);
            int sinh = (int) (sin * h2);
            int cosw = (int) (cos * w2);
            int cosh = (int) (cos * h2);
            // 左上
            int pos = i * 2;
            m_vertices[pos] = ox - sinw;
            m_vertices[pos + 1] = oy - cosh;
            // 左下
            pos += ndiv * 2;
            m_vertices[pos] = ox - cosw;
            m_vertices[pos + 1] = oy + sinh;
            // 右下
            pos += ndiv * 2;
            m_vertices[pos] = ox + sinw;
            m_vertices[pos + 1] = oy + cosh;
            // 右上
            pos += ndiv * 2;
            m_vertices[pos] = ox + cosw;
            m_vertices[pos + 1] = oy - sinh;
        }
        setFloatBuffer(buf, m_vertices, ndiv * 4 * 2);
    }

    private FloatBuffer createFloatBuffer(int count) {
        ByteBuffer bb = ByteBuffer.allocateDirect(count * FLOAT_BYTES * 2 * 2); // xyで2、描画先とテクスチャで2セット
        bb.order(ByteOrder.nativeOrder());
        return bb.asFloatBuffer();
    }

    void deviceToLogical(int[] coord, int count) {
        if (m_viewWidth - m_marginX * 2 == 0)
            return;

        float mag = (float) m_executor.getWidth() / (m_viewWidth - m_marginX * 2);
        for (int i = 0; i < count; i++) {
            coord[i * 2] = (int) ((coord[i * 2] - m_marginX) * mag);
            coord[i * 2 + 1] = (int) ((coord[i * 2 + 1] - m_marginY) * mag);
        }
    }

    @Override
    public void onDrawFrame(GL10 gl_) {
        {
            m_executor.doBackPressed();
            m_executor.doTouchEvent();
            m_executor.onUpdate();

            Timer timer = m_executor.getTimer();
            if (m_enableFrameSkip) {
                // 処理落ち気味ならフレームスキップ(5フレームに1回は描画しておく)
                int count = 0;
                while (count < 5 && timer.getElapsedTime() > 1000) {
                    timer.wait(1);
                    m_executor.onUpdate();
                    count++;
                }
            }

            GLES20.glEnable(GLES20.GL_BLEND);
            setNormalTransform();
            m_executor.onDraw();
            GLES20.glDisable(GLES20.GL_BLEND);

            setDirectTransform();
            // 余白
            int c = setColor(0xff000000);
            if (m_marginX > 0) {
                fillRect(0, 0, m_marginX, m_viewHeight);
                fillRect(m_viewWidth - m_marginX, 0, m_marginX, m_viewHeight);
            }
            if (m_marginY > 0) {
                fillRect(0, 0, m_viewWidth, m_marginY);
                fillRect(0, m_viewHeight - m_marginY, m_viewWidth, m_marginY);
            }
            // 負荷
            if (m_showLoad) {
                fillRect(0, 0, 204, 12);
                setColor(0xffffffff);
                drawRect(0, 0, 204, 12);
                int load = timer.getElapsedTime();
                if (load > 1000)
                    setColor(0xffff0000);
                else
                    setColor(0xff00ff00);
                fillRect(2, 2, 200 * load / 1000, 8);
            }
            setColor(c);

            timer.wait(1);
        }
    }

    private void drawSolidShape(int type, int count, boolean blend) {
        Shader shader;
        if (m_maskImage >= 0) {
            shader = m_shaderSM;
        } else {
            shader = m_shaderS;
        }
        GLES20.glUseProgram(shader.m_program);
        prepareSolidColorBlend(shader, blend);
        GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, m_bufferId);
        GLES20.glBufferData(GLES20.GL_ARRAY_BUFFER, FLOAT_BYTES * 2 * count, m_floatBuf, GLES20.GL_DYNAMIC_DRAW);

        GLES20.glVertexAttribPointer(shader.m_aPosition, 2, GLES20.GL_FLOAT, false, 0, 0);
        GLES20.glEnableVertexAttribArray(shader.m_aPosition);

        if (m_maskImage >= 0) {
            setMaskTransform(shader);
        }

        GLES20.glDrawArrays(type, 0, count);

        GLES20.glDisableVertexAttribArray(shader.m_aPosition);
        GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, 0);
    }

    private Shader getTexturedShader() {
        ColormapManager.Colormap colormap = m_executor.getColormap();
        if (m_maskImage >= 0) {
            if (colormap != null) {
                return m_shaderCM;
            } else {
                return m_shaderTM;
            }
        } else {
            if (colormap != null) {
                return m_shaderC;
            } else {
                return m_shaderT;
            }
        }
    }

    private void beginDrawTexture(Shader shader, int textureId, int textureWidth, int textureHeight) {
        GLES20.glUseProgram(shader.m_program);
        prepareTextureBlend(shader);

        GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, m_bufferId);
        GLES20.glEnableVertexAttribArray(shader.m_aPosition);
        GLES20.glEnableVertexAttribArray(shader.m_aTexPosition);

        m_floatVector[0] = textureWidth;
        m_floatVector[1] = textureHeight;
        GLES20.glUniform2fv(shader.m_uTexSize, 1, m_floatVector, 0);

        GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, textureId);
        GLES20.glUniform1i(shader.m_uSampler, 0);

        ColormapManager.Colormap colormap = m_executor.getColormap();
        if (colormap != null) {
            GLES20.glUniform1iv(shader.m_uColormapBorder, ColormapManager.Colormap.CAPACITY, colormap.getBorders(), 0);
            GLES20.glUniform4fv(shader.m_uColormapColor, ColormapManager.Colormap.CAPACITY, colormap.getColors(), 0);
            GLES20.glUniform1i(shader.m_uColormapCount, colormap.getCount());
            GLES20.glUniform1i(shader.m_uColormapOffset, colormap.getOffset());
        }

        if (m_maskImage >= 0) {
            setMaskTransform(shader);
        }
    }

    private void endDrawTexture(Shader shader) {
        GLES20.glDisableVertexAttribArray(shader.m_aPosition);
        GLES20.glDisableVertexAttribArray(shader.m_aTexPosition);
        GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, 0);
    }

    private void setNormalTransform() {
        float lw = m_executor.getWidth();
        float lh = m_executor.getHeight();

        // 座標計算
        // magX = (m_viewWidth - m_marginX * 2) / lw;
        // magY = (m_viewHeight - m_marginY * 2) / lh;
        // x' = (x * magX + m_marginX) / m_viewWidth * 2f - 1f;
        // y' = (m_viewHeight - m_marginY - y * magY) / m_viewHeight * 2f - 1f;
        m_floatVector[0] = (2f - 4f * m_marginX / m_viewWidth) / lw;
        m_floatVector[1] = (4f * m_marginY / m_viewHeight - 2f) / lh;
        m_floatVector[2] = 2f * m_marginX / m_viewWidth - 1f;
        m_floatVector[3] = 1f - 2f * m_marginY / m_viewHeight;

        GLES20.glUseProgram(m_shaderS.m_program);
        GLES20.glUniform2fv(m_shaderS.m_uTransform, 2, m_floatVector, 0);
        GLES20.glUseProgram(m_shaderT.m_program);
        GLES20.glUniform2fv(m_shaderT.m_uTransform, 2, m_floatVector, 0);
        GLES20.glUseProgram(m_shaderC.m_program);
        GLES20.glUniform2fv(m_shaderC.m_uTransform, 2, m_floatVector, 0);
        GLES20.glUseProgram(m_shaderSM.m_program);
        GLES20.glUniform2fv(m_shaderSM.m_uTransform, 2, m_floatVector, 0);
        GLES20.glUseProgram(m_shaderTM.m_program);
        GLES20.glUniform2fv(m_shaderTM.m_uTransform, 2, m_floatVector, 0);
        GLES20.glUseProgram(m_shaderCM.m_program);
        GLES20.glUniform2fv(m_shaderCM.m_uTransform, 2, m_floatVector, 0);
    }

    private void setDirectTransform() {
        // m_viewWidth, m_viewHeightの座標系で計算
        m_floatVector[0] = 2f / m_viewWidth;
        m_floatVector[1] = -2f / m_viewHeight;
        m_floatVector[2] = -1f;
        m_floatVector[3] = 1f;

        GLES20.glUseProgram(m_shaderS.m_program);
        GLES20.glUniform2fv(m_shaderS.m_uTransform, 2, m_floatVector, 0);
        GLES20.glUseProgram(m_shaderT.m_program);
        GLES20.glUniform2fv(m_shaderT.m_uTransform, 2, m_floatVector, 0);
        GLES20.glUseProgram(m_shaderC.m_program);
        GLES20.glUniform2fv(m_shaderC.m_uTransform, 2, m_floatVector, 0);
        GLES20.glUseProgram(m_shaderSM.m_program);
        GLES20.glUniform2fv(m_shaderSM.m_uTransform, 2, m_floatVector, 0);
        GLES20.glUseProgram(m_shaderTM.m_program);
        GLES20.glUniform2fv(m_shaderTM.m_uTransform, 2, m_floatVector, 0);
        GLES20.glUseProgram(m_shaderCM.m_program);
        GLES20.glUniform2fv(m_shaderCM.m_uTransform, 2, m_floatVector, 0);
    }

    private void setMaskTransform(Shader shader) {
        Texture tex = m_texture[m_maskImage];
        int tw = tex.getWidth();
        int th = tex.getHeight();
        m_floatVector[0] = (float) m_maskSw / m_maskDw / tw;
        m_floatVector[1] = (float) m_maskSh / m_maskDh / th;
        m_floatVector[2] = (m_maskSx - (float) m_maskDx * m_maskSw / m_maskDw) / tw;
        m_floatVector[3] = (m_maskSy - (float) m_maskDy * m_maskSh / m_maskDh) / th;
        GLES20.glUniform2fv(shader.m_uMaskTransform, 2, m_floatVector, 0);

        m_floatVector[0] = (float) m_maskSx / tw;
        m_floatVector[1] = (float) m_maskSy / th;
        m_floatVector[2] = (float) (m_maskSx + m_maskSw) / tw;
        m_floatVector[3] = (float) (m_maskSy + m_maskSh) / th;
        GLES20.glUniform2fv(shader.m_uMaskRange, 2, m_floatVector, 0);

        GLES20.glActiveTexture(GLES20.GL_TEXTURE1);
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, tex.getTextureId());
        GLES20.glUniform1i(shader.m_uMaskSampler, 1);
    }

    private static final String SRC_VS_S =
            "attribute vec2 a_Position;\n" +
                    "uniform vec2 u_Transform[2];\n" +
                    "void main() {\n" +
                    "  gl_Position = vec4(u_Transform[0] * a_Position + u_Transform[1], 0, 1);\n" +
                    "}\n";

    private static final String SRC_FS_S =
            "precision mediump float;\n" +
                    "uniform lowp vec4 u_FragColor;\n" +
                    "void main() {\n" +
                    "  gl_FragColor = u_FragColor;\n" +
                    "}\n";

    private static final String SRC_VS_T =
            "attribute vec2 a_Position;\n" +
                    "uniform vec2 u_Transform[2];\n" +
                    "attribute vec2 a_TexPosition;\n" +
                    "uniform vec2 u_TexSize;\n" +
                    "varying vec2 v_TexCoord;\n" +
                    "void main() {\n" +
                    "  gl_Position = vec4(u_Transform[0] * a_Position + u_Transform[1], 0, 1);\n" +
                    "  v_TexCoord = a_TexPosition / u_TexSize;\n" +
                    "}\n";

    private static final String SRC_FS_T =
            "#define API_LEVEL " + API_LEVEL + "\n" +
                    "precision mediump float;\n" +
                    "varying vec2 v_TexCoord;\n" +
                    "uniform sampler2D u_Sampler;\n" +
                    "uniform lowp vec4 u_FragColor;\n" +
                    "void main() {\n" +
                    "#if API_LEVEL < 19\n" +
                    "  vec4 color = texture2D(u_Sampler, v_TexCoord);\n" +
                    "  color.rgb /= (color.a + 0.000001);\n" +
                    "  gl_FragColor = color * u_FragColor;\n" +
                    "#else\n" +
                    "  gl_FragColor = texture2D(u_Sampler, v_TexCoord) * u_FragColor;\n" +
                    "#endif\n" +
                    "}\n";

    private static final String SRC_VS_C =
            "attribute vec2 a_Position;\n" +
                    "uniform vec2 u_Transform[2];\n" +
                    "attribute vec2 a_TexPosition;\n" +
                    "uniform vec2 u_TexSize;\n" +
                    "varying vec2 v_TexCoord;\n" +
                    "void main() {\n" +
                    "  gl_Position = vec4(u_Transform[0] * a_Position + u_Transform[1], 0, 1);\n" +
                    "  v_TexCoord = a_TexPosition / u_TexSize;\n" +
                    "}\n";

    private static final String SRC_FS_C =
            "#define API_LEVEL " + API_LEVEL + "\n" +
                    "precision mediump float;\n" +
                    "varying vec2 v_TexCoord;\n" +
                    "uniform sampler2D u_Sampler;\n" +
                    "uniform lowp vec4 u_FragColor;\n" +
                    "uniform lowp int u_ColormapBorder[8];\n" +
                    "uniform lowp vec4 u_ColormapColor[8];\n" +
                    "uniform lowp int u_ColormapOffset;\n" +
                    "void main() {\n" +
                    "  lowp vec4 color = texture2D(u_Sampler, v_TexCoord);\n" +
                    "#if API_LEVEL < 19\n" +
                    "  color.rgb /= (color.a + 0.000001);\n" +
                    "#endif\n" +
                    "  const vec4 luminance_vec = vec4(76.245, 149.685, 29.07, 0.0);\n" +
                    "  lowp int luminance = int(mod(dot(color, luminance_vec) + float(u_ColormapOffset), 256.0));\n" +
                    // 配列の添え字には変数が使えないので全パターン分岐する
                    "  lowp int b1;\n" +
                    "  lowp int b2;\n" +
                    "  lowp vec4 c1;\n" +
                    "  lowp vec4 c2;\n" +
                    "  if (luminance > u_ColormapBorder[3]) {\n" +
                    "    if (luminance > u_ColormapBorder[5]) {\n" +
                    "      if (luminance > u_ColormapBorder[6]) {\n" +
                    "        b1 = u_ColormapBorder[6];\n" +
                    "        b2 = u_ColormapBorder[7];\n" +
                    "        c1 = u_ColormapColor[6];\n" +
                    "        c2 = u_ColormapColor[7];\n" +
                    "      } else {\n" +
                    "        b1 = u_ColormapBorder[5];\n" +
                    "        b2 = u_ColormapBorder[6];\n" +
                    "        c1 = u_ColormapColor[5];\n" +
                    "        c2 = u_ColormapColor[6];\n" +
                    "      }\n" +
                    "    } else {\n" +
                    "      if (luminance > u_ColormapBorder[4]) {\n" +
                    "        b1 = u_ColormapBorder[4];\n" +
                    "        b2 = u_ColormapBorder[5];\n" +
                    "        c1 = u_ColormapColor[4];\n" +
                    "        c2 = u_ColormapColor[5];\n" +
                    "      } else {\n" +
                    "        b1 = u_ColormapBorder[3];\n" +
                    "        b2 = u_ColormapBorder[4];\n" +
                    "        c1 = u_ColormapColor[3];\n" +
                    "        c2 = u_ColormapColor[4];\n" +
                    "      }\n" +
                    "    }\n" +
                    "  } else {\n" +
                    "    if (luminance > u_ColormapBorder[1]) {\n" +
                    "      if (luminance > u_ColormapBorder[2]) {\n" +
                    "        b1 = u_ColormapBorder[2];\n" +
                    "        b2 = u_ColormapBorder[3];\n" +
                    "        c1 = u_ColormapColor[2];\n" +
                    "        c2 = u_ColormapColor[3];\n" +
                    "      } else {\n" +
                    "        b1 = u_ColormapBorder[1];\n" +
                    "        b2 = u_ColormapBorder[2];\n" +
                    "        c1 = u_ColormapColor[1];\n" +
                    "        c2 = u_ColormapColor[2];\n" +
                    "      }\n" +
                    "    } else {\n" +
                    "        b1 = u_ColormapBorder[0];\n" +
                    "        b2 = u_ColormapBorder[1];\n" +
                    "        c1 = u_ColormapColor[0];\n" +
                    "        c2 = u_ColormapColor[1];\n" +
                    "    }\n" +
                    "  }\n" +
                    "  float t = float(luminance - b1) / (float(b2 - b1) + 0.000001);\n" + // 0割り対策
                    "  vec4 mapColor = mix(c1, c2, t) * u_FragColor;\n" +
                    "  mapColor.a *= color.a;\n" +
                    "  gl_FragColor = mapColor;\n" +
                    "}\n";

    private static final String SRC_VS_SM =
            "attribute vec2 a_Position;\n" +
                    "uniform vec2 u_Transform[2];\n" +
                    "uniform vec2 u_MaskTransform[2];\n" +
                    "varying vec2 v_MaskCoord;\n" +
                    "void main() {\n" +
                    "  gl_Position = vec4(u_Transform[0] * a_Position + u_Transform[1], 0, 1);\n" +
                    "  v_MaskCoord = u_MaskTransform[0] * a_Position + u_MaskTransform[1];\n" +
                    "}\n";

    private static final String SRC_FS_SM =
            "precision mediump float;\n" +
                    "uniform lowp vec4 u_FragColor;\n" +
                    "varying vec2 v_MaskCoord;\n" +
                    "uniform vec2 u_MaskRange[2];\n" +
                    "uniform sampler2D u_MaskSampler;\n" +
                    "void main() {\n" +
                    // discaardより後にtexture2DがあるとフリーズするGPUがあるらしいので透明にしてreturn
                    //"  if (any(lessThan(v_MaskCoord, u_MaskRange[0])) || any(greaterThanEqual(v_MaskCoord, u_MaskRange[1]))) discard;\n" +
                    "  if (any(lessThan(v_MaskCoord, u_MaskRange[0])) || any(greaterThanEqual(v_MaskCoord, u_MaskRange[1]))) {\n" +
                    "    gl_FragColor = vec4(0.0, 0.0, 0.0, 0.0);\n" +
                    "    return;\n" +
                    "  }\n" +
                    "  vec4 color = u_FragColor;\n" +
                    "  color.a *= texture2D(u_MaskSampler, v_MaskCoord).a;\n" +
                    "  gl_FragColor = color;\n" +
                    "}\n";

    private static final String SRC_VS_TM =
            "attribute vec2 a_Position;\n" +
                    "uniform vec2 u_Transform[2];\n" +
                    "attribute vec2 a_TexPosition;\n" +
                    "uniform vec2 u_TexSize;\n" +
                    "varying vec2 v_TexCoord;\n" +
                    "uniform vec2 u_MaskTransform[2];\n" +
                    "varying vec2 v_MaskCoord;\n" +
                    "void main() {\n" +
                    "  gl_Position = vec4(u_Transform[0] * a_Position + u_Transform[1], 0, 1);\n" +
                    "  v_TexCoord = a_TexPosition / u_TexSize;\n" +
                    "  v_MaskCoord = u_MaskTransform[0] * a_Position + u_MaskTransform[1];\n" +
                    "}\n";

    private static final String SRC_FS_TM =
            "#define API_LEVEL " + API_LEVEL + "\n" +
                    "precision mediump float;\n" +
                    "varying vec2 v_TexCoord;\n" +
                    "uniform sampler2D u_Sampler;\n" +
                    "uniform lowp vec4 u_FragColor;\n" +
                    "varying vec2 v_MaskCoord;\n" +
                    "uniform vec2 u_MaskRange[2];\n" +
                    "uniform sampler2D u_MaskSampler;\n" +
                    "void main() {\n" +
                    // discaardより後にtexture2DがあるとフリーズするGPUがあるらしいので透明にしてreturn
                    //"  if (any(lessThan(v_MaskCoord, u_MaskRange[0])) || any(greaterThanEqual(v_MaskCoord, u_MaskRange[1]))) discard;\n" +
                    "  if (any(lessThan(v_MaskCoord, u_MaskRange[0])) || any(greaterThanEqual(v_MaskCoord, u_MaskRange[1]))) {\n" +
                    "    gl_FragColor = vec4(0.0, 0.0, 0.0, 0.0);\n" +
                    "    return;\n" +
                    "  }\n" +
                    "#if API_LEVEL < 19\n" +
                    "  vec4 color = texture2D(u_Sampler, v_TexCoord);\n" +
                    "  color.rgb /= (color.a + 0.000001);\n" +
                    "  color *= u_FragColor;\n" +
                    "#else\n" +
                    "  vec4 color = texture2D(u_Sampler, v_TexCoord) * u_FragColor;\n" +
                    "#endif\n" +
                    "  color.a *= texture2D(u_MaskSampler, v_MaskCoord).a;\n" +
                    "  gl_FragColor = color;\n" +
                    "}\n";

    private static final String SRC_VS_CM =
            "attribute vec2 a_Position;\n" +
                    "uniform vec2 u_Transform[2];\n" +
                    "attribute vec2 a_TexPosition;\n" +
                    "uniform vec2 u_TexSize;\n" +
                    "varying vec2 v_TexCoord;\n" +
                    "uniform vec2 u_MaskTransform[2];\n" +
                    "varying vec2 v_MaskCoord;\n" +
                    "void main() {\n" +
                    "  gl_Position = vec4(u_Transform[0] * a_Position + u_Transform[1], 0, 1);\n" +
                    "  v_TexCoord = a_TexPosition / u_TexSize;\n" +
                    "  v_MaskCoord = u_MaskTransform[0] * a_Position + u_MaskTransform[1];\n" +
                    "}\n";

    private static final String SRC_FS_CM =
            "#define API_LEVEL " + API_LEVEL + "\n" +
                    "precision mediump float;\n" +
                    "varying vec2 v_TexCoord;\n" +
                    "uniform sampler2D u_Sampler;\n" +
                    "uniform lowp vec4 u_FragColor;\n" +
                    "uniform lowp int u_ColormapBorder[8];\n" +
                    "uniform lowp vec4 u_ColormapColor[8];\n" +
                    "uniform lowp int u_ColormapOffset;\n" +
                    "varying vec2 v_MaskCoord;\n" +
                    "uniform vec2 u_MaskRange[2];\n" +
                    "uniform sampler2D u_MaskSampler;\n" +
                    "void main() {\n" +
                    // discaardより後にtexture2DがあるとフリーズするGPUがあるらしいので透明にしてreturn
                    //"  if (any(lessThan(v_MaskCoord, u_MaskRange[0])) || any(greaterThanEqual(v_MaskCoord, u_MaskRange[1]))) discard;\n" +
                    "  if (any(lessThan(v_MaskCoord, u_MaskRange[0])) || any(greaterThanEqual(v_MaskCoord, u_MaskRange[1]))) {\n" +
                    "    gl_FragColor = vec4(0.0, 0.0, 0.0, 0.0);\n" +
                    "    return;\n" +
                    "  }\n" +
                    "  lowp vec4 color = texture2D(u_Sampler, v_TexCoord);\n" +
                    "#if API_LEVEL < 19\n" +
                    "  color.rgb /= (color.a + 0.000001);\n" +
                    "#endif\n" +
                    "  const vec4 luminance_vec = vec4(76.245, 149.685, 29.07, 0.0);\n" +
                    "  lowp int luminance = int(mod(dot(color, luminance_vec) + float(u_ColormapOffset), 256.0));\n" +
                    // 配列の添え字には変数が使えないので全パターン分岐する
                    "  lowp int b1;\n" +
                    "  lowp int b2;\n" +
                    "  lowp vec4 c1;\n" +
                    "  lowp vec4 c2;\n" +
                    "  if (luminance > u_ColormapBorder[3]) {\n" +
                    "    if (luminance > u_ColormapBorder[5]) {\n" +
                    "      if (luminance > u_ColormapBorder[6]) {\n" +
                    "        b1 = u_ColormapBorder[6];\n" +
                    "        b2 = u_ColormapBorder[7];\n" +
                    "        c1 = u_ColormapColor[6];\n" +
                    "        c2 = u_ColormapColor[7];\n" +
                    "      } else {\n" +
                    "        b1 = u_ColormapBorder[5];\n" +
                    "        b2 = u_ColormapBorder[6];\n" +
                    "        c1 = u_ColormapColor[5];\n" +
                    "        c2 = u_ColormapColor[6];\n" +
                    "      }\n" +
                    "    } else {\n" +
                    "      if (luminance > u_ColormapBorder[4]) {\n" +
                    "        b1 = u_ColormapBorder[4];\n" +
                    "        b2 = u_ColormapBorder[5];\n" +
                    "        c1 = u_ColormapColor[4];\n" +
                    "        c2 = u_ColormapColor[5];\n" +
                    "      } else {\n" +
                    "        b1 = u_ColormapBorder[3];\n" +
                    "        b2 = u_ColormapBorder[4];\n" +
                    "        c1 = u_ColormapColor[3];\n" +
                    "        c2 = u_ColormapColor[4];\n" +
                    "      }\n" +
                    "    }\n" +
                    "  } else {\n" +
                    "    if (luminance > u_ColormapBorder[1]) {\n" +
                    "      if (luminance > u_ColormapBorder[2]) {\n" +
                    "        b1 = u_ColormapBorder[2];\n" +
                    "        b2 = u_ColormapBorder[3];\n" +
                    "        c1 = u_ColormapColor[2];\n" +
                    "        c2 = u_ColormapColor[3];\n" +
                    "      } else {\n" +
                    "        b1 = u_ColormapBorder[1];\n" +
                    "        b2 = u_ColormapBorder[2];\n" +
                    "        c1 = u_ColormapColor[1];\n" +
                    "        c2 = u_ColormapColor[2];\n" +
                    "      }\n" +
                    "    } else {\n" +
                    "        b1 = u_ColormapBorder[0];\n" +
                    "        b2 = u_ColormapBorder[1];\n" +
                    "        c1 = u_ColormapColor[0];\n" +
                    "        c2 = u_ColormapColor[1];\n" +
                    "    }\n" +
                    "  }\n" +
                    "  float t = float(luminance - b1) / (float(b2 - b1) + 0.000001);\n" + // 0割り対策
                    "  vec4 mapColor = mix(c1, c2, t) * u_FragColor;\n" +
                    "  mapColor.a *= color.a * texture2D(u_MaskSampler, v_MaskCoord).a;\n" +
                    "  gl_FragColor = mapColor;\n" +
                    "}\n";

    private int compileShader(int type, String source) {
        int shader = GLES20.glCreateShader(type);
        GLES20.glShaderSource(shader, source);
        GLES20.glCompileShader(shader);
        int[] result = new int[1];
        GLES20.glGetShaderiv(shader, GLES20.GL_COMPILE_STATUS, result, 0);
        if (result[0] == GLES20.GL_FALSE) {
            int error = GLES20.glGetError();
            String message = "Shader compile error : " + GLES20.glGetShaderInfoLog(shader);
            Log.e("Vermont", message);
            throw new GLException(error, message);
        }
        return shader;
    }

    private Shader createShader(String vertexShader, String fragmentShader, int flags) {
        Shader shader = new Shader();
        int program = GLES20.glCreateProgram();
        shader.m_program = program;
        String apiLevel = String.valueOf(Build.VERSION.SDK_INT);
        GLES20.glAttachShader(program, compileShader(GLES20.GL_VERTEX_SHADER, vertexShader.replace(API_LEVEL, apiLevel)));
        GLES20.glAttachShader(program, compileShader(GLES20.GL_FRAGMENT_SHADER, fragmentShader.replace(API_LEVEL, apiLevel)));
        GLES20.glLinkProgram(program);
        GLES20.glUseProgram(program);
        if ((flags & Shader.POSITION) != 0)
            shader.m_aPosition = GLES20.glGetAttribLocation(program, "a_Position");
        if ((flags & Shader.TEX_POSITION) != 0)
            shader.m_aTexPosition = GLES20.glGetAttribLocation(program, "a_TexPosition");
        if ((flags & Shader.TRANSFORM) != 0)
            shader.m_uTransform = GLES20.glGetUniformLocation(program, "u_Transform");
        if ((flags & Shader.SAMPLER) != 0)
            shader.m_uSampler = GLES20.glGetUniformLocation(program, "u_Sampler");
        if ((flags & Shader.FRAG_COLOR) != 0)
            shader.m_uFragColor = GLES20.glGetUniformLocation(program, "u_FragColor");
        if ((flags & Shader.TEX_SIZE) != 0)
            shader.m_uTexSize = GLES20.glGetUniformLocation(program, "u_TexSize");
        if ((flags & Shader.TEX_SIZE) != 0)
            shader.m_uTexSize = GLES20.glGetUniformLocation(program, "u_TexSize");
        if ((flags & Shader.COLORMAP_BORDER) != 0)
            shader.m_uColormapBorder = GLES20.glGetUniformLocation(program, "u_ColormapBorder");
        if ((flags & Shader.COLORMAP_COLOR) != 0)
            shader.m_uColormapColor = GLES20.glGetUniformLocation(program, "u_ColormapColor");
        if ((flags & Shader.COLORMAP_OFFSET) != 0)
            shader.m_uColormapOffset = GLES20.glGetUniformLocation(program, "u_ColormapOffset");
        if ((flags & Shader.MASK_TRANSFORM) != 0)
            shader.m_uMaskTransform = GLES20.glGetUniformLocation(program, "u_MaskTransform");
        if ((flags & Shader.MASK_SAMPLER) != 0)
            shader.m_uMaskSampler = GLES20.glGetUniformLocation(program, "u_MaskSampler");
        if ((flags & Shader.MASK_RANGE) != 0)
            shader.m_uMaskRange = GLES20.glGetUniformLocation(program, "u_MaskRange");

        return shader;
    }

    @Override
    public void onSurfaceChanged(GL10 gl_, int width, int height) {
        float lw = m_executor.getWidth();
        float lh = m_executor.getHeight();
        m_marginX = 0;
        m_marginY = 0;
        if (lw * height > lh * width) {
            // 画面の方が縦長
            m_marginY = (int) ((height - width * lh / lw) / 2);
        } else {
            // 画面の方が横長
            m_marginX = (int) ((width - height * lw / lh) / 2);
        }

        m_viewWidth = width;
        m_viewHeight = height;
        GLES20.glViewport(0, 0, width, height);
    }

    @Override
    public void onSurfaceCreated(GL10 gl_, EGLConfig config) {
        GLES20.glDisable(GLES20.GL_DITHER);
        GLES20.glClearColor(0f, 0f, 0f, 1f);
        GLES20.glDisable(GLES20.GL_CULL_FACE);
        GLES20.glDisable(GLES20.GL_DEPTH_TEST);

        // シェーダの初期化
        m_shaderS = createShader(SRC_VS_S, SRC_FS_S, Shader.POSITION | Shader.TRANSFORM | Shader.FRAG_COLOR);
        m_shaderT = createShader(SRC_VS_T, SRC_FS_T, Shader.POSITION | Shader.TEX_POSITION | Shader.TRANSFORM | Shader.SAMPLER | Shader.FRAG_COLOR | Shader.TEX_SIZE);
        m_shaderC = createShader(SRC_VS_C, SRC_FS_C, Shader.POSITION | Shader.TEX_POSITION | Shader.TRANSFORM | Shader.SAMPLER | Shader.FRAG_COLOR | Shader.TEX_SIZE |
                Shader.COLORMAP_BORDER | Shader.COLORMAP_COLOR | Shader.COLORMAP_OFFSET);
        m_shaderSM = createShader(SRC_VS_SM, SRC_FS_SM, Shader.POSITION | Shader.TRANSFORM | Shader.FRAG_COLOR | Shader.MASK_TRANSFORM | Shader.MASK_SAMPLER | Shader.MASK_RANGE);
        m_shaderTM = createShader(SRC_VS_TM, SRC_FS_TM, Shader.POSITION | Shader.TEX_POSITION | Shader.TRANSFORM | Shader.SAMPLER | Shader.FRAG_COLOR | Shader.TEX_SIZE |
                Shader.MASK_TRANSFORM | Shader.MASK_SAMPLER | Shader.MASK_RANGE);
        m_shaderCM = createShader(SRC_VS_CM, SRC_FS_CM, Shader.POSITION | Shader.TEX_POSITION | Shader.TRANSFORM | Shader.SAMPLER | Shader.FRAG_COLOR | Shader.TEX_SIZE |
                Shader.COLORMAP_BORDER | Shader.COLORMAP_COLOR | Shader.COLORMAP_OFFSET | Shader.MASK_TRANSFORM | Shader.MASK_SAMPLER | Shader.MASK_RANGE);

        // Attribute用バッファ
        int[] buf = new int[1];
        GLES20.glGenBuffers(1, buf, 0);
        m_bufferId = buf[0];

        // テクスチャIDの初期化とリロード
        for (int i = 0; i < m_textureMax; i++) {
            m_texture[i].init();
            m_texture[i].reload();
        }
        m_textTexture.init();
        m_textTexture.reload();
    }
}
