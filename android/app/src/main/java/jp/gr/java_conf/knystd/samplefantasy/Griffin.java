package jp.gr.java_conf.knystd.samplefantasy;
import android.app.Activity;

import java.util.ArrayList;
import java.util.Random;

import jp.gr.java_conf.knystd.vermont.*;
import jp.gr.java_conf.knystd.vermont.fukujin.*;
import jp.gr.java_conf.knystd.vermont.util.*;


/*------------------------------------------------------------------------------ */
/* Griffin */
/* グリフォン */
/*------------------------------------------------------------------------------ */
public class Griffin extends Enemy {

    public Griffin(double x, double y, int number) {
        super(x, y, 160, 160, 4, number);
    }

    @Override
    public void draw(VermontExecutor ex) {
        int x = this.getDispX();
        int y = this.getDispY();
        this.drawEnemy(ex, x, y, 1, 317, 160, 160);
        this.drawNumber(ex, x + 80, y + 100, this.getNumber());
        this.drawHp(ex, x + 80, y + 170, this.getHp(), this.getMaxHp());
    }

}
