package jp.gr.java_conf.knystd.samplefantasy;
import android.app.Activity;

import java.util.ArrayList;
import java.util.Random;

import jp.gr.java_conf.knystd.vermont.*;
import jp.gr.java_conf.knystd.vermont.fukujin.*;
import jp.gr.java_conf.knystd.vermont.util.*;


/*------------------------------------------------------------------------------ */
/* EnemyData */
/* 敵配置データ */
/*------------------------------------------------------------------------------ */
public class EnemyData {

    public EnemyData(int enemyType, int x, int y) {
        this.m_enemyType = enemyType;
        this.m_x = x;
        this.m_y = y;
    }

    public int getEnemyType() {
        return this.m_enemyType;
    }

    public void setEnemyType(int enemyType) {
        this.m_enemyType = enemyType;
    }

    public int getX() {
        return this.m_x;
    }

    public void setX(int x) {
        this.m_x = x;
    }

    public int getY() {
        return this.m_y;
    }

    public void setY(int y) {
        this.m_y = y;
    }

    protected int m_enemyType;
    protected int m_x;
    protected int m_y;
}
