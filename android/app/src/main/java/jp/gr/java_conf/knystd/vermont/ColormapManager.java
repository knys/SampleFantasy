package jp.gr.java_conf.knystd.vermont;

class ColormapManager {
    class Colormap {
        static final int CAPACITY = 8;

        private int[] m_border;
        private float[] m_color;
        private int m_offset;

        Colormap(int count) {
            m_border = new int[CAPACITY];
            m_color = new float[CAPACITY * 4];
            m_offset = 0;
            for (int i = count; i < CAPACITY; i++) {
                // 未使用分に最大値をセット
                m_border[i] = 255;
            }
        }

        void setValue(int position, int border, int color) {
            if (position < 0 || position >= m_border.length)
                return;

            m_border[position] = border;
            m_color[position * 4] = (color >>> 16 & 0x0ff) / 255f;
            m_color[position * 4 + 1] = (color >>> 8 & 0x0ff) / 255f;
            m_color[position * 4 + 2] = (color & 0x0ff) / 255f;
            m_color[position * 4 + 3] = (color >>> 24) / 255f;
        }

        void setOffset(int offset) {
            m_offset = offset;
        }

        int[] getBorders() {
            return m_border;
        }

        float[] getColors() {
            return m_color;
        }

        int getOffset() {
            return m_offset;
        }

        int getCount() {
            return m_border.length;
        }
    }

    private int m_colormapMax = 0;
    private Colormap[] m_colormaps;

    ColormapManager(int colormapMax) {
        m_colormapMax = colormapMax;
        m_colormaps = new Colormap[colormapMax];
        init();
    }

    void init() {
        for (int i = 0; i < m_colormapMax; i++)
            m_colormaps[i] = null;
    }

    int createColormap(int count, int index) {
        if (index < -1 || index >= m_colormapMax)
            return -1;

        if (index < 0) {
            for (int i = 0; i < m_colormapMax; i++) {
                if (m_colormaps[i] == null) {
                    index = i;
                    break;
                }
            }
        }

        if (index < 0)
            return -1;

        freeColormap(index);

        m_colormaps[index] = new Colormap(count);

        return index;
    }

    void freeColormap(int index) {
        if (index < 0 || index >= m_colormapMax)
            return;

        m_colormaps[index] = null;
    }

    Colormap getColormap(int index) {
        if (index < 0 || index >= m_colormapMax)
            return null;

        return m_colormaps[index];
    }
}
