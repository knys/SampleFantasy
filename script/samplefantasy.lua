import("class")
import("array")
import("java")
import("fukujin")
import("textreader")
import("textwriter")
import("gamecharacter")
import("drawutil")

function onStart()
    local ex = SampleFantasyExecutor.new()
    ex:onStart()
end

--------------------------------------------------------------------------------
-- SampleFantasyExecutor
-- VermontのAPI実行を受け持つクラス
--------------------------------------------------------------------------------
class("SampleFantasyExecutor", VermontExecutor)

function SampleFantasyExecutor:ctor()
    SampleFantasyExecutor.super.ctor(self)
    self.m_app = nil --(SampleFantasyApp)
end

function SampleFantasyExecutor:onStart()
    self.m_app = SampleFantasyApp.new(self)
end

function SampleFantasyExecutor:onLoad()
end

function SampleFantasyExecutor:onUpdate()
    self.m_app:onUpdate()
end

function SampleFantasyExecutor:onDraw()
    self.m_app:onDraw()
end

function SampleFantasyExecutor:onTouchEvent(action, count, coord, id) --(int, int, int[], int[])
    self.m_app:onTouchEvent(action, count, coord, id)
end

function SampleFantasyExecutor:onBackPressed() --{boolean}
    return self.m_app:onBackPressed()
end

--------------------------------------------------------------------------------
-- SampleFantasyApp
-- アプリの骨格となるクラス
--------------------------------------------------------------------------------
class("SampleFantasyApp", Application)

-- 画面の論理解像度
SampleFantasyApp.WIN_WIDTH = 480
SampleFantasyApp.WIN_HEIGHT = 700

-- 画面の種類
SampleFantasyApp.SCENE_TITLE = 0 -- タイトル画面
SampleFantasyApp.SCENE_GAME = 1 -- ゲーム画面

-- 画像
SampleFantasyApp.IMG_MAIN = 1

-- カラーマップ
SampleFantasyApp.CM_DAMAGE = 1

function SampleFantasyApp:ctor(ex) --(VermontExecutor)
    SampleFantasyApp.super.ctor(self, ex)
    self.m_level = 1 --(int)
    self.m_bestTime = Array.new("int", 3) --(int[])

    ex:setResolution(SampleFantasyApp.WIN_WIDTH, SampleFantasyApp.WIN_HEIGHT) -- 論理解像度設定
    ex:setFps(60) -- 秒間60コマで動く

    ex:loadImage("samplefantasy.png", SampleFantasyApp.IMG_MAIN)
    self:loadData()

    ex:createColormap(3, SampleFantasyApp.CM_DAMAGE)
    ex:setColormapValue(SampleFantasyApp.CM_DAMAGE, 0, 0, ex:rgb(64, 0, 0))
    ex:setColormapValue(SampleFantasyApp.CM_DAMAGE, 1, 128, ex:rgb(255, 160, 160))
    ex:setColormapValue(SampleFantasyApp.CM_DAMAGE, 2, 255, ex:rgb(255, 255, 255))

    self:setScene(SampleFantasyApp.SCENE_TITLE) -- タイトル画面を表示
end

function SampleFantasyApp:getLevel() --{int}
    return self.m_level
end

function SampleFantasyApp:setLevel(level) --(int)
    self.m_level = level
end

function SampleFantasyApp:getBestTime(level) --{int}(int)
    return self.m_bestTime[level - 1]
end

function SampleFantasyApp:setBestTime(level, bestTime) --(int, int)
    self.m_bestTime[level - 1] = bestTime
end

function SampleFantasyApp:setScene(scene) --(int)
    -- 指定された画面に切り替える
    if scene == SampleFantasyApp.SCENE_TITLE then
        self:setForm(TitleForm.new(0, 0, SampleFantasyApp.WIN_WIDTH, SampleFantasyApp.WIN_HEIGHT, self))
    elseif scene == SampleFantasyApp.SCENE_GAME then
        self:setForm(GameForm.new(0, 0, SampleFantasyApp.WIN_WIDTH, SampleFantasyApp.WIN_HEIGHT, self))
    end
end

function SampleFantasyApp:loadData()
    -- 初期化
    for i = 0, self.m_bestTime.length - 1 do
        self.m_bestTime[i] = 999 * 60
    end

    local tr = TextReader.new() --(TextReader)
    if tr:open(TextReader.MODE_PRIVATE, self:getExecutor():getActivity(), "besttime.dat") then
        local index = 0 --(int)
        local s = tr:readLine() --(String)
        while s ~= nil and index < self.m_bestTime.length do
            self.m_bestTime[index] = Integer.parseInt(s)
            index = index + 1
            s = tr:readLine()
        end
        tr:close()
    end
end

function SampleFantasyApp:saveData()
    local tw = TextWriter.new() --(TextWriter)
    if tw:open(TextReader.MODE_PRIVATE, self:getExecutor():getActivity(), "besttime.dat") then
        for i = 0, self.m_bestTime.length - 1 do
            tw:writeLine(String.valueOf(self.m_bestTime[i]))
        end
        tw:close()
    end
end

--------------------------------------------------------------------------------
-- TitleButton
-- タイトル画面用ボタン
--------------------------------------------------------------------------------
class("TitleButton", Button)

function TitleButton:ctor(parent, x, y, width, height, level, app) --(Widget, int, int, int, int, int, SampleFantasyApp)
    TitleButton.super.ctor(self, parent, x, y, width, height)
    self.m_level = level --(int)
    self.m_app = app --(SampleFantasyApp)
end

--@Override
function TitleButton:onDraw(ex) --(VermontExecutor)
    -- ボタンの描画
    local x = self:getDispX() --(int)
    local y = self:getDispY() --(int)
    local w = self:getWidth() --(int)
    local h = self:getHeight() --(int)

    if self:isFocused() then
        x = x - 5
        y = y - 5
        w = w + 10
        h = h + 10
    end

    ex:stretchBlt(x, y, w, h, SampleFantasyApp.IMG_MAIN, 365, 433, 100, 60)
end

--@Override
function TitleButton:onClick()
    -- クリックされたらゲーム画面に切り替える
    self.m_app:setLevel(self.m_level)
    self.m_app:setScene(SampleFantasyApp.SCENE_GAME)
end

--------------------------------------------------------------------------------
-- TitleForm
-- タイトル画面
--------------------------------------------------------------------------------
class("TitleForm", Form)

function TitleForm:ctor(x, y, width, height, app) --(int, int, int, int, SampleFantasyApp)
    TitleForm.super.ctor(self, x, y, width, height)

    self.m_app = app --(SampleFantasyApp)

    -- 画面にボタンを追加する
    self:addChild(TitleButton.new(self, 350, 410, 100, 60, 1, app))
    self:addChild(TitleButton.new(self, 350, 510, 100, 60, 2, app))
    self:addChild(TitleButton.new(self, 350, 610, 100, 60, 3, app))
end

--@Override
function TitleForm:onTouchEvent(action, count, coord, id) --(int, int, int[], int[])
end

--@Override
function TitleForm:onDraw(ex) --(VermontExecutor)
    -- タイトル描画
    ex:cls(ex:rgb(24, 48, 96))
    local x = self:getDispX() --(int)
    local y = self:getDispY() --(int)
    ex:blt(x, y + 120, SampleFantasyApp.IMG_MAIN, 543, 883, 480, 140)

    ex:blt(x + 20, y + 410, SampleFantasyApp.IMG_MAIN, 365, 495, 120, 60)
    ex:blt(x, y + 390, SampleFantasyApp.IMG_MAIN, 543, 522, 480, 100)
    DrawUtil.drawTime(ex, x + 180, y + 440, self.m_app:getBestTime(1))
    ex:blt(x + 20, y + 510, SampleFantasyApp.IMG_MAIN, 365, 557, 120, 60)
    ex:blt(x, y + 490, SampleFantasyApp.IMG_MAIN, 543, 522, 480, 100)
    DrawUtil.drawTime(ex, x + 180, y + 540, self.m_app:getBestTime(2))
    ex:blt(x + 20, y + 610, SampleFantasyApp.IMG_MAIN, 365, 619, 120, 60)
    ex:blt(x, y + 590, SampleFantasyApp.IMG_MAIN, 543, 522, 480, 100)
    DrawUtil.drawTime(ex, x + 180, y + 640, self.m_app:getBestTime(3))
end

--@Override
function TitleForm:onBackPressed() --{boolean}
    -- タイトル画面でバックボタンを押したら終了する
    return true
end

--------------------------------------------------------------------------------
-- GameForm
-- ゲーム画面
--------------------------------------------------------------------------------
class("GameForm", Form)

function GameForm:ctor(x, y, width, height, app) --(int, int, int, int, SampleFantasyApp)
    GameForm.super.ctor(self, x, y, width, height)

    self.m_app = app --(SampleFantasyApp)
    self.m_gameCore = GameCore.new(app:getExecutor()) --(GameCore)
end

--@Override
function GameForm:onInit()
    self.m_gameCore:init(self.m_app:getLevel())
end

--@Override
function GameForm:onClose()
    self.m_gameCore:close()
end

--@Override
function GameForm:onUpdate()
    GameForm.super.onUpdate(self)

    -- 1フレーム分の更新処理
    self.m_gameCore:update()

    if self.m_gameCore:getMode() == GameCore.MODE_END then
        if self.m_gameCore:isCleared() then
            local time = self.m_gameCore:getTime() --(int)
            local level = self.m_app:getLevel() --(int)
            if time < self.m_app:getBestTime(level) then
                self.m_app:setBestTime(level, time)
                self.m_app:saveData()
            end
        end
        self.m_app:setScene(SampleFantasyApp.SCENE_TITLE)
    end
end

--@Override
function GameForm:onTouchEvent(action, count, coord, id) --(int, int, int[], int[])
    local x = coord[0] --(int)
    local y = coord[1] --(int)
    if action == VermontExecutor.ACTION_DOWN then
        self.m_gameCore:attack(x, y)
    end
end

--@Override
function GameForm:onDraw(ex) --(VermontExecutor)
    -- ゲーム画面描画
    self.m_gameCore:draw()
end

--@Override
function GameForm:onBackPressed() --{boolean}
    -- タイトル画面に戻る
    self.m_app:setScene(SampleFantasyApp.SCENE_TITLE)
    -- アプリは終了しないのでfalseを返す
    return false
end
